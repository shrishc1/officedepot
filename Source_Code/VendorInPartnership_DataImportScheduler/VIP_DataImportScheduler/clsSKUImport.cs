﻿using System.Data.SqlClient;
using System.IO;
using System;
using System.Data;
using System.Text;
using Utilities;

namespace VendorInPartnership_DataImportScheduler
{
    class clsSKUImport
    {
        public void UploadItem(string itemFilePath, string itemFileName)
        {
            DataTable dt = new DataTable();
            DataRow dr;
            int TotalRecordCount = 0;

            int Qty_Sold_YesterdayIndex = 0;
            int Qty_On_HandIndex = 0;
            int qty_on_backorderIndex = 0;
            int BalanceIndex = 0;
            int Item_ValIndex = 0;
            int Product_Lead_timeIndex = 0;
            int Leadtime_VarianceIndex = 0;
            int BackOrderIndex = 0;
            int Valuated_StockIndex = 0;

            //------------------------------------

            DataTable errorDt = new DataTable();
            DataRow errorDr;

            int Direct_SKU_Index = 0;
            int OD_SKU_NO_Index = 0;
            int DESCRIPTION_Index = 0;
            int Vendor_Code_Index = 0;
            int Warehouse_Index = 0;
            int Date_Index = 0;
            int Currency_Index = 0;
            int Vendor_no_Index = 0;
            int Vendor_Name_Index = 0;
            int subvndr_Index = 0;
            int UOM_Index = 0;
            int Buyer_no_Index = 0;
            int Item_Category_Index = 0;
            int Item_classification_Index = 0;
            int New_Item_Index = 0;
            int ICASUN_Index = 0;
            int ILAYUN_Index = 0;
            int IPALUN_Index = 0;
            int IMINQT_Index = 0;
            int IBMULT_Index = 0;
            int Category_Index = 0;
            int Country_Index = 0;
            int Total_Qty_BO_Index = 0;
            int Total_Count_BO_Index = 0;

            int Total_Orders_Index = 0;
            int Total_Sales_Index = 0;

            //-------------------------------------

            string FPID = Common.InsertUP_FileProcessed(GlobalVariable.folderDownloadedID.Value, TotalRecordCount, 0, itemFileName);
            int fileProcessedID = Convert.ToInt32(FPID);
            try
            {
                using (StreamReader oStreamReader = new StreamReader(itemFilePath, System.Text.Encoding.Default))
                {
                    GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                    var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                    foreach (string col in columns)
                    {
                        switch (col.ToLower())
                        {
                            case "direct_sku":
                                {
                                    dt.Columns.Add(new DataColumn("Direct_SKU"));
                                    Direct_SKU_Index = Array.IndexOf(columns, "direct_sku");
                                }
                                break;
                            case "od_sku_no":
                                {
                                    dt.Columns.Add(new DataColumn("OD_SKU_NO"));
                                    OD_SKU_NO_Index = Array.IndexOf(columns, "od_sku_no");
                                }
                                break;
                            case "description":
                                dt.Columns.Add(new DataColumn("DESCRIPTION"));
                                DESCRIPTION_Index = Array.IndexOf(columns, "description");
                                break;
                            case "vendor_code":
                                dt.Columns.Add(new DataColumn("Vendor_Code"));
                                Vendor_Code_Index = Array.IndexOf(columns, "vendor_code");
                                break;
                            case "warehouse":
                                dt.Columns.Add(new DataColumn("Warehouse"));
                                Warehouse_Index = Array.IndexOf(columns, "warehouse");
                                break;
                            case "date1":
                                dt.Columns.Add(new DataColumn("Date"));
                                Date_Index = Array.IndexOf(columns, "date1");
                                break;
                            case "qty_sold_yesterday":
                                dt.Columns.Add(new DataColumn("Qty_Sold_Yesterday", typeof(System.Decimal)));
                                Qty_Sold_YesterdayIndex = Array.IndexOf(columns, "qty_sold_yesterday");
                                break;
                            case "qty_on_hand":
                                dt.Columns.Add(new DataColumn("Qty_On_Hand", typeof(System.Decimal)));
                                Qty_On_HandIndex = Array.IndexOf(columns, "qty_on_hand");
                                break;
                            case "qty_on_backorder":
                                dt.Columns.Add(new DataColumn("qty_on_backorder", typeof(System.Decimal)));
                                qty_on_backorderIndex = Array.IndexOf(columns, "qty_on_backorder");
                                break;
                            case "balance":
                                dt.Columns.Add(new DataColumn("Balance", typeof(System.Int32)));
                                BalanceIndex = Array.IndexOf(columns, "balance");
                                break;
                            case "item_val":
                                dt.Columns.Add(new DataColumn("Item_Val", typeof(System.Decimal)));
                                Item_ValIndex = Array.IndexOf(columns, "item_val");
                                break;
                            case "item_val_gbp":
                                dt.Columns.Add(new DataColumn("Item_Val", typeof(System.Decimal)));
                                Item_ValIndex = Array.IndexOf(columns, "item_val_gbp");
                                break;
                            case "valuated_stock":
                                dt.Columns.Add(new DataColumn("Valuated_Stock"));
                                Valuated_StockIndex = Array.IndexOf(columns, "valuated_stock");
                                break;
                            case "currency":
                                dt.Columns.Add(new DataColumn("Currency"));
                                Currency_Index = Array.IndexOf(columns, "currency");
                                break;
                            case "vendor_no":
                                dt.Columns.Add(new DataColumn("Vendor_no"));
                                Vendor_no_Index = Array.IndexOf(columns, "vendor_no");
                                break;
                            case "vendor_name":
                                dt.Columns.Add(new DataColumn("Vendor_Name"));
                                Vendor_Name_Index = Array.IndexOf(columns, "vendor_name");
                                break;
                            case "subvndr":
                                dt.Columns.Add(new DataColumn("subvndr"));
                                subvndr_Index = Array.IndexOf(columns, "subvndr");
                                break;
                            case "product_lead_time":
                                dt.Columns.Add(new DataColumn("Product_Lead_time", typeof(System.Int32)));
                                Product_Lead_timeIndex = Array.IndexOf(columns, "product_lead_time");
                                break;
                            case "leadtime_variance":
                                dt.Columns.Add(new DataColumn("Leadtime_Variance", typeof(System.Int32)));
                                Leadtime_VarianceIndex = Array.IndexOf(columns, "leadtime_variance");
                                break;
                            case "uom":
                                dt.Columns.Add(new DataColumn("UOM"));
                                UOM_Index = Array.IndexOf(columns, "uom");
                                break;
                            case "buyer_no":
                                dt.Columns.Add(new DataColumn("Buyer_no"));
                                Buyer_no_Index = Array.IndexOf(columns, "buyer_no");
                                break;
                            case "item_category":
                                dt.Columns.Add(new DataColumn("Item_Category"));
                                Item_Category_Index = Array.IndexOf(columns, "item_category");
                                break;
                            case "item_classification":
                                dt.Columns.Add(new DataColumn("Item_classification"));
                                Item_classification_Index = Array.IndexOf(columns, "item_classification");
                                break;
                            case "new_item":
                                dt.Columns.Add(new DataColumn("New_Item"));
                                New_Item_Index = Array.IndexOf(columns, "new_item");
                                break;
                            case "icasun":
                                dt.Columns.Add(new DataColumn("ICASUN"));
                                ICASUN_Index = Array.IndexOf(columns, "icasun");
                                break;
                            case "ilayun":
                                dt.Columns.Add(new DataColumn("ILAYUN"));
                                ILAYUN_Index = Array.IndexOf(columns, "ilayun");
                                break;
                            case "ipalun":
                                dt.Columns.Add(new DataColumn("IPALUN"));
                                IPALUN_Index = Array.IndexOf(columns, "ipalun");
                                break;
                            case "iminqt":
                                dt.Columns.Add(new DataColumn("IMINQT"));
                                IMINQT_Index = Array.IndexOf(columns, "iminqt");
                                break;
                            case "ibmult":
                                dt.Columns.Add(new DataColumn("IBMULT"));
                                IBMULT_Index = Array.IndexOf(columns, "ibmult");
                                break;
                            case "category":
                                dt.Columns.Add(new DataColumn("Category"));
                                Category_Index = Array.IndexOf(columns, "category");
                                break;
                            case "back order":
                                dt.Columns.Add(new DataColumn("Back Order", typeof(System.Int32)));
                                BackOrderIndex = Array.IndexOf(columns, "back order");
                                break;
                            case "backorder":
                                dt.Columns.Add(new DataColumn("Back Order", typeof(System.Int32)));
                                BackOrderIndex = Array.IndexOf(columns, "backorder");
                                break;
                            case "back_order":
                                dt.Columns.Add(new DataColumn("Back Order", typeof(System.Int32)));
                                BackOrderIndex = Array.IndexOf(columns, "back_order");
                                break;
                            case "country":
                                dt.Columns.Add(new DataColumn("Country"));
                                Country_Index = Array.IndexOf(columns, "country");
                                break;
                            case "total_qty_bo":
                                dt.Columns.Add(new DataColumn("Total_Qty_BO", typeof(System.Decimal)));
                                Total_Qty_BO_Index = Array.IndexOf(columns, "total_qty_bo");
                                break;
                            case "total_count_bo":
                                dt.Columns.Add(new DataColumn("Total_Count_BO", typeof(System.Decimal)));
                                Total_Count_BO_Index = Array.IndexOf(columns, "total_count_bo");
                                break;

                            case "total_orders":
                                dt.Columns.Add(new DataColumn("Total_Orders", typeof(System.Decimal)));
                                Total_Orders_Index = Array.IndexOf(columns, "total_orders");
                                break;
                            case "total_sales":
                                dt.Columns.Add(new DataColumn("Total_Sales", typeof(System.Decimal)));
                                Total_Sales_Index = Array.IndexOf(columns, "total_sales");
                                break;

                            default:
                                break;
                        }
                    }
                    dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));

                    dt.Columns.Add(new DataColumn("Daily_Value", typeof(System.Decimal)));
                    dt.Columns.Add(new DataColumn("Total_Value", typeof(System.Decimal)));

                    //--------------------- Added on 14 Feb 2013 -----------------------
                    errorDt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ManualFileID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                    errorDt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                    //--------------------- Added on 14 Feb 2013 -----------------------

                    int iCount = 0;
                    DateTime? tempDate = DateTime.Now;
                    while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                    {
                        string errorMessage = string.Empty;
                        errorDr = errorDt.NewRow();


                        iCount++;

                        dr = dt.NewRow();
                        GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                        //if (GlobalVariable.textFileFieldValues.Length != dt.Columns.Count - 1) {
                        if (GlobalVariable.textFileFieldValues.Length != dt.Columns.Count - 3)
                        {
                            StringBuilder sbMessage = new StringBuilder();
                            sbMessage.Append("\r\n");
                            sbMessage.Append("Data Incomplete at position " + iCount.ToString());
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                            continue;
                        }
                        for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                        {

                            if (i == Vendor_no_Index)
                            {
                                var Vendor_No = GlobalVariable.textFileFieldValues[Vendor_no_Index];

                                if (GlobalVariable.textFileFieldValues[Country_Index].ToLower() == ("UKandIRE").ToLower()
                                    && Vendor_No.Length >= 5 && Vendor_No.Substring(0, 2) == "00")
                                {
                                    Vendor_No = Vendor_No.Substring(2);
                                }
                                GlobalVariable.textFileFieldValues[i] = Vendor_No;
                            }
                            GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                        }

                        GlobalVariable.textFileFieldValues[BalanceIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[BalanceIndex]);
                        GlobalVariable.textFileFieldValues[Product_Lead_timeIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Product_Lead_timeIndex]);
                        GlobalVariable.textFileFieldValues[Leadtime_VarianceIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Leadtime_VarianceIndex]);
                        GlobalVariable.textFileFieldValues[BackOrderIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[BackOrderIndex]);

                        GlobalVariable.textFileFieldValues[Qty_Sold_YesterdayIndex] = RemoveSpace(GlobalVariable.textFileFieldValues[Qty_Sold_YesterdayIndex]);
                        GlobalVariable.textFileFieldValues[Qty_On_HandIndex] = RemoveSpace(GlobalVariable.textFileFieldValues[Qty_On_HandIndex]);
                        GlobalVariable.textFileFieldValues[qty_on_backorderIndex] = RemoveSpace(GlobalVariable.textFileFieldValues[qty_on_backorderIndex]);
                        GlobalVariable.textFileFieldValues[Item_ValIndex] = RemoveSpace(GlobalVariable.textFileFieldValues[Item_ValIndex]);
                        GlobalVariable.textFileFieldValues[Valuated_StockIndex] = RemoveSpace(GlobalVariable.textFileFieldValues[Valuated_StockIndex]);

                        GlobalVariable.textFileFieldValues[Total_Qty_BO_Index] = RemoveSpace(GlobalVariable.textFileFieldValues[Total_Qty_BO_Index]);
                        GlobalVariable.textFileFieldValues[Total_Count_BO_Index] = RemoveSpace(GlobalVariable.textFileFieldValues[Total_Count_BO_Index]);

                        GlobalVariable.textFileFieldValues[Total_Orders_Index] = RemoveSpace(GlobalVariable.textFileFieldValues[Total_Orders_Index]);
                        GlobalVariable.textFileFieldValues[Total_Sales_Index] = RemoveSpace(GlobalVariable.textFileFieldValues[Total_Sales_Index]);

                        //--------------------- Added on 14 Feb 2013 - Start ---------------- 

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Direct_SKU_Index])) && GlobalVariable.textFileFieldValues[Direct_SKU_Index].Length > 25)
                        {

                            errorMessage += GetErrorMessage(columns[Direct_SKU_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Direct_SKU_Index],
                                                                          25);
                            GlobalVariable.textFileFieldValues[Direct_SKU_Index] = GlobalVariable.textFileFieldValues[Direct_SKU_Index].Substring(0, 25);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[OD_SKU_NO_Index])) && GlobalVariable.textFileFieldValues[OD_SKU_NO_Index].Length > 25)
                        {

                            errorMessage += GetErrorMessage(columns[OD_SKU_NO_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[OD_SKU_NO_Index],
                                                                          25);

                            GlobalVariable.textFileFieldValues[OD_SKU_NO_Index] = GlobalVariable.textFileFieldValues[OD_SKU_NO_Index].Substring(0, 25);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[DESCRIPTION_Index])) && GlobalVariable.textFileFieldValues[DESCRIPTION_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[DESCRIPTION_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[DESCRIPTION_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[DESCRIPTION_Index] = GlobalVariable.textFileFieldValues[DESCRIPTION_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_Code_Index])) && GlobalVariable.textFileFieldValues[Vendor_Code_Index].Length > 50)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_Code_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_Code_Index],
                                                                          50);
                            GlobalVariable.textFileFieldValues[Vendor_Code_Index] = GlobalVariable.textFileFieldValues[Vendor_Code_Index].Substring(0, 50);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Warehouse_Index])) && GlobalVariable.textFileFieldValues[Warehouse_Index].Length > 10)
                        {

                            errorMessage += GetErrorMessage(columns[Warehouse_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Warehouse_Index],
                                                                          10);
                            GlobalVariable.textFileFieldValues[Warehouse_Index] = GlobalVariable.textFileFieldValues[Warehouse_Index].Substring(0, 10);
                        }

                        //if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Date_Index])) && GlobalVariable.textFileFieldValues[Date_Index].Length > 25)
                        //{

                        //    errorMessage += GetErrorMessage(columns[Date_Index].ToString(),
                        //                                                  GlobalVariable.textFileFieldValues[Date_Index],
                        //                                                  25);
                        //    GlobalVariable.textFileFieldValues[Date_Index] = GlobalVariable.textFileFieldValues[Date_Index].Substring(0, 25);
                        //}


                        try
                        {
                            tempDate = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[Date_Index]));
                            GlobalVariable.textFileFieldValues[Date_Index] = GlobalVariable.textFileFieldValues[Date_Index];
                        }
                        catch
                        {
                            errorMessage += columns[Date_Index].ToString() + " has invalid date - " + GlobalVariable.textFileFieldValues[Date_Index] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + itemFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }


                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Valuated_StockIndex]) && GlobalVariable.textFileFieldValues[Valuated_StockIndex].Length > 10)
                        {

                            errorMessage += GetErrorMessage(columns[Valuated_StockIndex].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Valuated_StockIndex],
                                                                          10);
                            GlobalVariable.textFileFieldValues[Valuated_StockIndex] = GlobalVariable.textFileFieldValues[Valuated_StockIndex].Substring(0, 10);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Currency_Index]) && GlobalVariable.textFileFieldValues[Currency_Index].Length > 10)
                        {

                            errorMessage += GetErrorMessage(columns[Currency_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Currency_Index],
                                                                          10);
                            GlobalVariable.textFileFieldValues[Currency_Index] = GlobalVariable.textFileFieldValues[Currency_Index].Substring(0, 10);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_no_Index]) && GlobalVariable.textFileFieldValues[Vendor_no_Index].Length > 15)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_no_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Vendor_no_Index],
                                                                         15);
                            GlobalVariable.textFileFieldValues[Vendor_no_Index] = GlobalVariable.textFileFieldValues[Vendor_no_Index].Substring(0, 15);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_Name_Index]) && GlobalVariable.textFileFieldValues[Vendor_Name_Index].Length > 50)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_Name_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_Name_Index],
                                                                          50);
                            GlobalVariable.textFileFieldValues[Vendor_Name_Index] = GlobalVariable.textFileFieldValues[Vendor_Name_Index].Substring(0, 50);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[subvndr_Index]) && GlobalVariable.textFileFieldValues[subvndr_Index].Length > 5)
                        {

                            errorMessage += GetErrorMessage(columns[subvndr_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[subvndr_Index],
                                                                          5);
                            GlobalVariable.textFileFieldValues[subvndr_Index] = GlobalVariable.textFileFieldValues[subvndr_Index].Substring(0, 5);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[UOM_Index]) && GlobalVariable.textFileFieldValues[UOM_Index].Length > 5)
                        {

                            errorMessage += GetErrorMessage(columns[UOM_Index].ToString(),
                                                                           GlobalVariable.textFileFieldValues[UOM_Index],
                                                                           5);
                            GlobalVariable.textFileFieldValues[UOM_Index] = GlobalVariable.textFileFieldValues[UOM_Index].Substring(0, 5);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Buyer_no_Index]) && GlobalVariable.textFileFieldValues[Buyer_no_Index].Length > 20)
                        {

                            errorMessage += GetErrorMessage(columns[Buyer_no_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Buyer_no_Index],
                                                                          20);
                            GlobalVariable.textFileFieldValues[Buyer_no_Index] = GlobalVariable.textFileFieldValues[Buyer_no_Index].Substring(0, 20);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Item_Category_Index]) && GlobalVariable.textFileFieldValues[Item_Category_Index].Length > 10)
                        {

                            errorMessage += GetErrorMessage(columns[Item_Category_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Item_Category_Index],
                                                                          10);
                            GlobalVariable.textFileFieldValues[Item_Category_Index] = GlobalVariable.textFileFieldValues[Item_Category_Index].Substring(0, 10);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Item_classification_Index]) && GlobalVariable.textFileFieldValues[Item_classification_Index].Length > 5)
                        {

                            errorMessage += GetErrorMessage(columns[Item_classification_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Item_classification_Index],
                                                                         5);
                            GlobalVariable.textFileFieldValues[Item_classification_Index] = GlobalVariable.textFileFieldValues[Item_classification_Index].Substring(0, 5);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[New_Item_Index]) && GlobalVariable.textFileFieldValues[New_Item_Index].Length > 15)
                        {
                            errorMessage += GetErrorMessage(columns[New_Item_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[New_Item_Index],
                                                                          15);
                            GlobalVariable.textFileFieldValues[New_Item_Index] = GlobalVariable.textFileFieldValues[New_Item_Index].Substring(0, 15);
                        }


                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[ICASUN_Index]) && GlobalVariable.textFileFieldValues[ICASUN_Index].Length > 15)
                        {
                            errorMessage += GetErrorMessage(columns[ICASUN_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[ICASUN_Index],
                                                                          15);
                            GlobalVariable.textFileFieldValues[ICASUN_Index] = GlobalVariable.textFileFieldValues[ICASUN_Index].Substring(0, 15);
                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[ILAYUN_Index]) && GlobalVariable.textFileFieldValues[ILAYUN_Index].Length > 15)
                        {
                            errorMessage += GetErrorMessage(columns[ILAYUN_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[ILAYUN_Index],
                                                                          15);
                            GlobalVariable.textFileFieldValues[ILAYUN_Index] = GlobalVariable.textFileFieldValues[ILAYUN_Index].Substring(0, 15);

                        }

                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[IPALUN_Index]) && GlobalVariable.textFileFieldValues[IPALUN_Index].Length > 15)
                        {
                            errorMessage += GetErrorMessage(columns[IPALUN_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[IPALUN_Index],
                                                                          15);
                            GlobalVariable.textFileFieldValues[IPALUN_Index] = GlobalVariable.textFileFieldValues[IPALUN_Index].Substring(0, 15);

                        }
                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[IMINQT_Index]) && GlobalVariable.textFileFieldValues[IMINQT_Index].Length > 15)
                        {
                            errorMessage += GetErrorMessage(columns[IMINQT_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[IMINQT_Index],
                                                                          15);
                            GlobalVariable.textFileFieldValues[IMINQT_Index] = GlobalVariable.textFileFieldValues[IMINQT_Index].Substring(0, 15);

                        }
                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[IBMULT_Index]) && GlobalVariable.textFileFieldValues[IBMULT_Index].Length > 15)
                        {

                            errorMessage += GetErrorMessage(columns[IBMULT_Index].ToString(),
                                                                           GlobalVariable.textFileFieldValues[IBMULT_Index],
                                                                           15);
                            GlobalVariable.textFileFieldValues[IBMULT_Index] = GlobalVariable.textFileFieldValues[IBMULT_Index].Substring(0, 15);

                        }
                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Category_Index]) && GlobalVariable.textFileFieldValues[Category_Index].Length > 15)
                        {

                            errorMessage += GetErrorMessage(columns[Category_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Category_Index],
                                                                          15);
                            GlobalVariable.textFileFieldValues[Category_Index] = GlobalVariable.textFileFieldValues[Category_Index].Substring(0, 15);

                        }
                        if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Country_Index]) && GlobalVariable.textFileFieldValues[Country_Index].Length > 20)
                        {
                            errorMessage += GetErrorMessage(columns[Country_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Country_Index],
                                                                          20);
                            GlobalVariable.textFileFieldValues[Country_Index] = GlobalVariable.textFileFieldValues[Country_Index].Substring(0, 20);
                        }



                        //--------------------- Added on 13 Feb 2013 - End ---------------- 

                        dr.ItemArray = GlobalVariable.textFileFieldValues;
                        dr["UpdatedDate"] = Common.GetFileDate(itemFileName); // new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                        dt.Rows.Add(dr);

                        //--------------------- Added on 14 Feb 2013 -----------------------
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + itemFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                        }
                        //--------------------- Added on 14 Feb 2013 -----------------------

                        TotalRecordCount += 1;
                        //GlobalVariable.sqlComm.CommandText = "INSERT INTO [UP_StageSKU] " +
                        //              "([Country],[Direct_SKU] ,[OD_SKU_NO] ,[DESCRIPTION] ,[Vendor_Code] ,[Warehouse] " +
                        //              ",[Date] ,[Qty_Sold_Yesterday],[Qty_On_HAND],[qty_on_backorder],[Balance] " +
                        //              ",[Item_Val],[Valuated_Stock],[Currency],[Vendor_no],[Vendor_Name] ,[subvndr] " +
                        //              ",[Product_Lead_time],[Leadtime_Variance],[UOM],[Buyer_no],[Item_Category] " +
                        //               ",[Item_classification],[New_Item],[ICASUN],[ILAYUN],[IPALUN]" +
                        //              ",[IMINQT],[IBMULT],[Category],[Back Order],UpdatedDate) values ('"+
                        //              GlobalVariable.textFileFieldValues[0] + "','" + GlobalVariable.textFileFieldValues[1] + "','" +
                        //              GlobalVariable.textFileFieldValues[2] + "','" + GlobalVariable.textFileFieldValues[3] + "','" +
                        //              GlobalVariable.textFileFieldValues[4] + "','" + GlobalVariable.textFileFieldValues[5] + "','" +
                        //              GlobalVariable.textFileFieldValues[6] + "','" + GlobalVariable.textFileFieldValues[7] + "','" +
                        //              GlobalVariable.textFileFieldValues[8] + "','" + GlobalVariable.textFileFieldValues[9] + "','" +
                        //              GlobalVariable.textFileFieldValues[10] + "','" + GlobalVariable.textFileFieldValues[11] + "','" +
                        //              GlobalVariable.textFileFieldValues[12] + "','" + GlobalVariable.textFileFieldValues[13] + "','" +
                        //              GlobalVariable.textFileFieldValues[14] + "','" + GlobalVariable.textFileFieldValues[15] + "','" +
                        //              GlobalVariable.textFileFieldValues[16] + "','" + GlobalVariable.textFileFieldValues[17] + "','" +
                        //              GlobalVariable.textFileFieldValues[18] + "','" + GlobalVariable.textFileFieldValues[19] + "','" +
                        //              GlobalVariable.textFileFieldValues[20] + "','" + GlobalVariable.textFileFieldValues[21] + "','" +
                        //              GlobalVariable.textFileFieldValues[22] + "','" + GlobalVariable.textFileFieldValues[23] + "','" +
                        //              GlobalVariable.textFileFieldValues[24] + "','" + GlobalVariable.textFileFieldValues[25] +"','" +
                        //              GlobalVariable.textFileFieldValues[26] + "','" + GlobalVariable.textFileFieldValues[27] + "','" +
                        //              GlobalVariable.textFileFieldValues[28] + "','" + GlobalVariable.textFileFieldValues[29] + "','" +
                        //              GlobalVariable.textFileFieldValues[30] + "','" + dr["UpdatedDate"] + "')";
                        // GlobalVariable.queryReturn = GlobalVariable.sqlComm.ExecuteNonQuery();                        
                    }
                    SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkCopy.DestinationTableName = "UP_StageSKU";

                    oSqlBulkCopy.ColumnMappings.Add("Direct_SKU", "Direct_SKU");
                    oSqlBulkCopy.ColumnMappings.Add("OD_SKU_NO", "OD_SKU_NO");
                    oSqlBulkCopy.ColumnMappings.Add("DESCRIPTION", "DESCRIPTION");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor_Code", "Vendor_Code");
                    oSqlBulkCopy.ColumnMappings.Add("Warehouse", "Warehouse");
                    oSqlBulkCopy.ColumnMappings.Add("Date", "Date");
                    oSqlBulkCopy.ColumnMappings.Add("Qty_Sold_Yesterday", "Qty_Sold_Yesterday");
                    oSqlBulkCopy.ColumnMappings.Add("Qty_On_Hand", "Qty_On_Hand");
                    oSqlBulkCopy.ColumnMappings.Add("qty_on_backorder", "qty_on_backorder");
                    oSqlBulkCopy.ColumnMappings.Add("Balance", "Balance");
                    oSqlBulkCopy.ColumnMappings.Add("Item_Val", "Item_Val");
                    oSqlBulkCopy.ColumnMappings.Add("Valuated_Stock", "Valuated_Stock");
                    oSqlBulkCopy.ColumnMappings.Add("Currency", "Currency");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor_no", "Vendor_no");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor_Name", "Vendor_Name");
                    oSqlBulkCopy.ColumnMappings.Add("subvndr", "subvndr");
                    oSqlBulkCopy.ColumnMappings.Add("Product_Lead_time", "Product_Lead_time");
                    oSqlBulkCopy.ColumnMappings.Add("Leadtime_Variance", "Leadtime_Variance");
                    oSqlBulkCopy.ColumnMappings.Add("UOM", "UOM");
                    oSqlBulkCopy.ColumnMappings.Add("Buyer_no", "Buyer_no");
                    oSqlBulkCopy.ColumnMappings.Add("Item_Category", "Item_Category");
                    oSqlBulkCopy.ColumnMappings.Add("Item_classification", "Item_classification");
                    oSqlBulkCopy.ColumnMappings.Add("New_Item", "New_Item");
                    oSqlBulkCopy.ColumnMappings.Add("ICASUN", "ICASUN");
                    oSqlBulkCopy.ColumnMappings.Add("ILAYUN", "ILAYUN");
                    oSqlBulkCopy.ColumnMappings.Add("IPALUN", "IPALUN");
                    oSqlBulkCopy.ColumnMappings.Add("IMINQT", "IMINQT");
                    oSqlBulkCopy.ColumnMappings.Add("IBMULT", "IBMULT");
                    oSqlBulkCopy.ColumnMappings.Add("Category", "Category");
                    oSqlBulkCopy.ColumnMappings.Add("Back Order", "Back Order");
                    oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                    oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkCopy.ColumnMappings.Add("Total_Qty_BO", "Total_Qty_BO");
                    oSqlBulkCopy.ColumnMappings.Add("Total_Count_BO", "Total_Count_BO");

                    oSqlBulkCopy.ColumnMappings.Add("Total_Orders", "Total_Orders");
                    oSqlBulkCopy.ColumnMappings.Add("Total_Sales", "Total_Sales");
                    //have to add two more fielsds which is added
                    oSqlBulkCopy.ColumnMappings.Add("Daily_Value", "Daily_Value");
                    oSqlBulkCopy.ColumnMappings.Add("Total_Value", "Total_Value");

                    oSqlBulkCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime; ;
                    oSqlBulkCopy.BatchSize = 5000;// dt.Rows.Count;
                    oSqlBulkCopy.WriteToServer(dt);
                    oSqlBulkCopy.Close();


                    //--------------------- Added on 14 Feb 2013 -----------------------
                    SqlBulkCopy oSqlBulkErrorCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkErrorCopy.DestinationTableName = "UP_ErrorSKU";

                    oSqlBulkErrorCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                    oSqlBulkErrorCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkErrorCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");

                    oSqlBulkErrorCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkErrorCopy.BatchSize = errorDt.Rows.Count;

                    oSqlBulkErrorCopy.WriteToServer(errorDt);
                    oSqlBulkErrorCopy.Close();
                    //--------------------- Added on 14 Feb 2013 -----------------------
                }
                Common.UpdateUP_FileProcessed(fileProcessedID, TotalRecordCount, 0);
            }
            catch (Exception ex)
            {
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
            }
            finally
            {
                //File.Delete(itemFilePath);
                //File.Delete(itemFilePath + ".gz");
            }

            /// -----------------------------------------------------
            /// --- Added on 19 Feb 2013 
            /// -----------------------------------------------------
            try
            {
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spUP_ManualFileUpload";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.VarChar).Value = "InsertFiles";
                GlobalVariable.sqlComm.Parameters.Add("@DateUploaded", SqlDbType.DateTime).Value = Common.GetFileDate(itemFileName);
                GlobalVariable.sqlComm.Parameters.Add("@DownloadedFilename", SqlDbType.VarChar).Value = itemFileName;
                GlobalVariable.sqlComm.Parameters.Add("@UserID", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalPORecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalReceiptInformationRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalSKURecord", SqlDbType.Int).Value = TotalRecordCount;
                GlobalVariable.sqlComm.Parameters.Add("@TotalVendorRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@UploadType", SqlDbType.VarChar).Value = "AUTO";
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;

                GlobalVariable.sqlComm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                GlobalVariable.sqlComm.CommandType = CommandType.Text;
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("No of Lines:" + TotalRecordCount);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            /// -----------------------------------------------------
        }

        private string RemoveDecimal(string val)
        {
            string retVal;
            retVal = RemoveSpace(val);

            if (retVal != null)
            {
                if (retVal.Contains("."))
                {
                    retVal = retVal.Substring(0, retVal.IndexOf("."));
                }
                if (retVal.Contains("-"))
                {
                    retVal = '-' + (retVal.Trim('-'));
                }
                if (retVal.Contains("/"))
                {
                    string[] arrDate = retVal.Split('/');
                    retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }
            }
            return retVal;
        }

        private string RemoveSpace(string val)
        {
            string retVal;
            if (!string.IsNullOrEmpty(val))
                retVal = val.Trim(' ');
            else
                retVal = null;
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="Description"></param>
        /// <param name="ColumnLength"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public string GetErrorMessage(string ColumnName, string Description, int ColumnLength)
        {
            var Message = ColumnName + " length greater than " + ColumnLength + " for " + Description + ",";
            return Message;
        }
    }
}

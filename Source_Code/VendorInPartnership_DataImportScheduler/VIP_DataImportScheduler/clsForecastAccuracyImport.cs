﻿using System.Data.SqlClient;
using System.IO;
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using Utilities;

namespace VendorInPartnership_DataImportScheduler
{
    class clsForecastAccuracyImport
    {
        public void UploadForecastAccuracy(string ExpediteFilePath, string ExpediteFileName) {

            DataTable dt = new DataTable();
            DataRow dr;
            int TotalRecordCount = 0;

            DataTable errorDt = new DataTable();
            DataRow errorDr;

            int Country_Index = 0;
            int OD_SKU_Index = 0;
            int Viking_SKU_Index = 0;
            int MRP_Index = 0;
            int PurGrp_Index = 0;
            int MatGroup_Index = 0;
            int StartingStock_Index = 0;
            int SafetyStock_Index = 0;
            int Site_Index = 0;
           
            int Month1_Index = 0;
            int Month2_Index = 0;
            int Month3_Index = 0;
            int Month4_Index = 0;
            int Month5_Index = 0;
            int Month6_Index = 0;
            int Month7_Index = 0;
            int Month8_Index = 0;
            int Month9_Index = 0;
            int Month10_Index = 0;
            int Month11_Index = 0;
            int Month12_Index = 0;

            StringBuilder sbMessage = new StringBuilder();
            int DataElementCount = 0;
       
            string FPID = Common.InsertUP_FileProcessed(GlobalVariable.folderDownloadedID.Value, TotalRecordCount, 0, ExpediteFileName);
            int fileProcessedID = Convert.ToInt32(FPID);
            try {
                using (StreamReader oStreamReader = new StreamReader(ExpediteFilePath))
                {
                    GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                    var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);


                    foreach (string col in columns)
                    {
                        switch (col.ToLower())
                        {
                            case "country":
                            dt.Columns.Add(new DataColumn("Country"));
                            Country_Index = Array.IndexOf(columns, "country");
                            break;
                            case "od sku":
                            dt.Columns.Add(new DataColumn("OD_SKU"));
                            OD_SKU_Index = Array.IndexOf(columns, "od sku");
                            break;
                            case "viking sku":
                            dt.Columns.Add(new DataColumn("Viking_SKU"));
                            Viking_SKU_Index = Array.IndexOf(columns, "viking sku");
                            break;
                            case "mrp":
                            dt.Columns.Add(new DataColumn("MRP"));
                            MRP_Index = Array.IndexOf(columns, "mrp");
                            break;
                            case "pur grp":
                            dt.Columns.Add(new DataColumn("PurGrp"));
                            PurGrp_Index = Array.IndexOf(columns, "pur grp");
                            break;
                            case "mat group":
                            dt.Columns.Add(new DataColumn("MatGroup"));
                            MatGroup_Index = Array.IndexOf(columns, "mat group");
                            break;
                            case "startingstock":
                            dt.Columns.Add(new DataColumn("StartingStock", typeof(System.Int32)));
                            StartingStock_Index = Array.IndexOf(columns, "startingstock");
                            break;
                            case "safety stock":
                            dt.Columns.Add(new DataColumn("SafetyStock", typeof(System.Int32)));
                            SafetyStock_Index = Array.IndexOf(columns, "safety stock");
                            break;
                            case "site":
                            dt.Columns.Add(new DataColumn("Site"));
                            Site_Index = Array.IndexOf(columns, "site");
                            break;

                            case "month1":
                            dt.Columns.Add(new DataColumn("Month1", typeof(System.Decimal)));
                            Month1_Index = Array.IndexOf(columns, "month1");
                            break;
                            case "month2":
                            dt.Columns.Add(new DataColumn("Month2", typeof(System.Decimal)));
                            Month2_Index = Array.IndexOf(columns, "month2");
                            break;
                            case "month3":
                            dt.Columns.Add(new DataColumn("Month3", typeof(System.Decimal)));
                            Month3_Index = Array.IndexOf(columns, "month3");
                            break;
                            case "month4":
                            dt.Columns.Add(new DataColumn("Month4", typeof(System.Decimal)));
                            Month4_Index = Array.IndexOf(columns, "month4");
                            break;
                            case "month5":
                            dt.Columns.Add(new DataColumn("Month5", typeof(System.Decimal)));
                            Month5_Index = Array.IndexOf(columns, "month5");
                            break;
                            case "month6":
                            dt.Columns.Add(new DataColumn("Month6", typeof(System.Decimal)));
                            Month6_Index = Array.IndexOf(columns, "month6");
                            break;
                            case "month7":
                            dt.Columns.Add(new DataColumn("Month7", typeof(System.Decimal)));
                            Month7_Index = Array.IndexOf(columns, "month7");
                            break;
                            case "month8":
                            dt.Columns.Add(new DataColumn("Month8", typeof(System.Decimal)));
                            Month8_Index = Array.IndexOf(columns, "month8");
                            break;
                            case "month9":
                            dt.Columns.Add(new DataColumn("Month9", typeof(System.Decimal)));
                            Month9_Index = Array.IndexOf(columns, "month9");
                            break;
                            case "month10":
                            dt.Columns.Add(new DataColumn("Month10", typeof(System.Decimal)));
                            Month10_Index = Array.IndexOf(columns, "month10");
                            break;
                            case "month11":
                            dt.Columns.Add(new DataColumn("Month11", typeof(System.Decimal)));
                            Month11_Index = Array.IndexOf(columns, "month11");
                            break;
                            case "month12":
                            dt.Columns.Add(new DataColumn("Month12", typeof(System.Decimal)));
                            Month12_Index = Array.IndexOf(columns, "month12");
                            break;                          
                            default:
                            break;
                        }

                    }
                    dt.Columns.Add(new DataColumn("FileDate", typeof(System.DateTime)));
                   
                    errorDt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ManualFileID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                    errorDt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                  

                    DateTime? tempDate = DateTime.Now;

                   

                    while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null) {                     

                        string errorMessage = string.Empty;
                        errorDr = errorDt.NewRow();
                        DataElementCount = 0;

                        dr = dt.NewRow();
                        GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);                        

                        if (GlobalVariable.textFileFieldValues.Length == 21) {

                            for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++) 
                            {
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");

                            }

                            try
                            {
                                GlobalVariable.textFileFieldValues[StartingStock_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[StartingStock_Index]);
                                DataElementCount++;
                            }
                            catch 
                            {
                                errorMessage += columns[StartingStock_Index].ToString() + " has invalid Starting Stock - " + GlobalVariable.textFileFieldValues[StartingStock_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            try
                            {
                                GlobalVariable.textFileFieldValues[SafetyStock_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[SafetyStock_Index]);
                                DataElementCount++;
                            }
                            catch 
                            {
                                errorMessage += columns[SafetyStock_Index].ToString() + " has invalid Safety Stock - " + GlobalVariable.textFileFieldValues[SafetyStock_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }
                            decimal temp;
                            bool IsConversionPassed = true;

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month1_Index], out temp);
                            if (IsConversionPassed) 
                            {
                                GlobalVariable.textFileFieldValues[Month1_Index] = GlobalVariable.textFileFieldValues[Month1_Index].Trim();
                                DataElementCount++;
                            }
                            else
                            {
                                errorMessage += columns[Month1_Index].ToString() + " has invalid stock on Month1 - " + GlobalVariable.textFileFieldValues[Month1_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month2_Index], out temp);
                            if (IsConversionPassed) 
                            {
                                GlobalVariable.textFileFieldValues[Month2_Index] = GlobalVariable.textFileFieldValues[Month2_Index].Trim();
                                DataElementCount++;
                            }
                            else 
                            {
                                errorMessage += columns[Month2_Index].ToString() + " has invalid stock on Month2 - " + GlobalVariable.textFileFieldValues[Month2_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month3_Index], out temp);
                            if (IsConversionPassed)
                            {
                                GlobalVariable.textFileFieldValues[Month3_Index] = GlobalVariable.textFileFieldValues[Month3_Index].Trim();
                                DataElementCount++;
                            }
                            else 
                            {
                                errorMessage += columns[Month3_Index].ToString() + " has invalid stock on Month3 - " + GlobalVariable.textFileFieldValues[Month3_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month4_Index], out temp);
                            if (IsConversionPassed)
                            {
                                GlobalVariable.textFileFieldValues[Month4_Index] = GlobalVariable.textFileFieldValues[Month4_Index].Trim();
                                DataElementCount++;
                            }
                            else
                            {
                                errorMessage += columns[Month4_Index].ToString() + " has invalid stock on Month4 - " + GlobalVariable.textFileFieldValues[Month4_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month5_Index], out temp);
                            if (IsConversionPassed) 
                            {
                                GlobalVariable.textFileFieldValues[Month5_Index] = GlobalVariable.textFileFieldValues[Month5_Index].Trim();
                                DataElementCount++;
                            }
                            else
                            {
                                errorMessage += columns[Month5_Index].ToString() + " has invalid stock on Month5 - " + GlobalVariable.textFileFieldValues[Month5_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month6_Index], out temp);
                            if (IsConversionPassed)
                            {
                                GlobalVariable.textFileFieldValues[Month6_Index] = GlobalVariable.textFileFieldValues[Month6_Index].Trim();
                                DataElementCount++;
                            }
                            else
                            {
                                errorMessage += columns[Month6_Index].ToString() + " has invalid stock on Month6 - " + GlobalVariable.textFileFieldValues[Month6_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month7_Index], out temp);
                            if (IsConversionPassed) 
                            {
                                GlobalVariable.textFileFieldValues[Month7_Index] = GlobalVariable.textFileFieldValues[Month7_Index].Trim();
                                DataElementCount++;
                            }
                            else
                            {
                                errorMessage += columns[Month7_Index].ToString() + " has invalid stock on Month7 - " + GlobalVariable.textFileFieldValues[Month7_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month8_Index], out temp);
                            if (IsConversionPassed) 
                            {
                                GlobalVariable.textFileFieldValues[Month8_Index] = GlobalVariable.textFileFieldValues[Month8_Index].Trim();
                                DataElementCount++;
                            }
                            else 
                            {
                                errorMessage += columns[Month8_Index].ToString() + " has invalid stock on Month8 - " + GlobalVariable.textFileFieldValues[Month8_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month9_Index], out temp);
                            if (IsConversionPassed)
                            {
                                GlobalVariable.textFileFieldValues[Month9_Index] = GlobalVariable.textFileFieldValues[Month9_Index].Trim();
                                DataElementCount++;
                            }
                            else 
                            {
                                errorMessage += columns[Month9_Index].ToString() + " has invalid stock on Month9 - " + GlobalVariable.textFileFieldValues[Month9_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month10_Index], out temp);
                            if (IsConversionPassed) 
                            {
                                GlobalVariable.textFileFieldValues[Month10_Index] = GlobalVariable.textFileFieldValues[Month10_Index].Trim();
                                DataElementCount++;
                            }
                            else 
                            {
                                errorMessage += columns[Month10_Index].ToString() + " has invalid stock on Month10 - " + GlobalVariable.textFileFieldValues[Month10_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month11_Index], out temp);
                            if (IsConversionPassed)
                            {
                                GlobalVariable.textFileFieldValues[Month11_Index] = GlobalVariable.textFileFieldValues[Month11_Index].Trim();
                                DataElementCount++;
                            }
                            else 
                            {
                                errorMessage += columns[Month11_Index].ToString() + " has invalid stock on Month11 - " + GlobalVariable.textFileFieldValues[Month11_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Month12_Index], out temp);
                            if (IsConversionPassed)
                            {
                                GlobalVariable.textFileFieldValues[Month12_Index] = GlobalVariable.textFileFieldValues[Month12_Index].Trim();
                                DataElementCount++;
                            }
                            else
                            {
                                errorMessage += columns[Month12_Index].ToString() + " has invalid stock on Month12 - " + GlobalVariable.textFileFieldValues[Month12_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            //--------------------- Added on 13 Feb 2013 - Start ---------------- 
                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Country_Index])) && GlobalVariable.textFileFieldValues[Country_Index].Length > 20)
                            {
                                errorMessage += GetErrorMessage(columns[Country_Index].ToString(),
                                                                              GlobalVariable.textFileFieldValues[Country_Index],
                                                                              20);
                                GlobalVariable.textFileFieldValues[Country_Index] = GlobalVariable.textFileFieldValues[Country_Index].Substring(0, 20);

                                DataElementCount++;
                            }
                            else
                            {
                                DataElementCount++;
                            }


                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[OD_SKU_Index])) && GlobalVariable.textFileFieldValues[OD_SKU_Index].Length > 25)
                            {
                                errorMessage += GetErrorMessage(columns[OD_SKU_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[OD_SKU_Index],
                                                                             25);
                                GlobalVariable.textFileFieldValues[OD_SKU_Index] = GlobalVariable.textFileFieldValues[OD_SKU_Index].Substring(0, 25);

                                DataElementCount++;
                            }
                            else
                            {
                                DataElementCount++;
                            }

                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Viking_SKU_Index])) && GlobalVariable.textFileFieldValues[Viking_SKU_Index].Length > 25) 
                            {
                                errorMessage += GetErrorMessage(columns[Viking_SKU_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[Viking_SKU_Index],
                                                                             25);
                                GlobalVariable.textFileFieldValues[Viking_SKU_Index] = GlobalVariable.textFileFieldValues[Viking_SKU_Index].Substring(0, 25);

                                DataElementCount++;
                            }
                            else 
                            {
                                DataElementCount++;
                            }


                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[MRP_Index])) && GlobalVariable.textFileFieldValues[MRP_Index].Length > 5)
                            {

                                errorMessage += GetErrorMessage(columns[MRP_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[MRP_Index],
                                                                             5);
                                GlobalVariable.textFileFieldValues[MRP_Index] = GlobalVariable.textFileFieldValues[MRP_Index].Substring(0, 5);

                                DataElementCount++;
                            }
                            else 
                            {
                                DataElementCount++;
                            }

                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[PurGrp_Index])) && GlobalVariable.textFileFieldValues[PurGrp_Index].Length > 5)
                            {

                                errorMessage += GetErrorMessage(columns[PurGrp_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[PurGrp_Index],
                                                                             5);
                                GlobalVariable.textFileFieldValues[PurGrp_Index] = GlobalVariable.textFileFieldValues[PurGrp_Index].Substring(0, 5);

                                DataElementCount++;
                            }
                            else
                            {
                                DataElementCount++;
                            }

                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[MatGroup_Index])) && GlobalVariable.textFileFieldValues[MatGroup_Index].Length > 5) 
                            {

                                errorMessage += GetErrorMessage(columns[MatGroup_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[MatGroup_Index],
                                                                             5);
                                GlobalVariable.textFileFieldValues[MatGroup_Index] = GlobalVariable.textFileFieldValues[MatGroup_Index].Substring(0, 5);

                                DataElementCount++;
                            }
                            else 
                            {
                                DataElementCount++;
                            }


                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Site_Index])) && GlobalVariable.textFileFieldValues[Site_Index].Length > 10)
                            {

                                errorMessage += GetErrorMessage(columns[Site_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[Site_Index],
                                                                             10);
                                GlobalVariable.textFileFieldValues[Site_Index] = GlobalVariable.textFileFieldValues[Site_Index].Substring(0, 10);

                                DataElementCount++;
                            }
                            else 
                            {
                                DataElementCount++;
                            }


                            //--------------------- Added on 13 Feb 2013 - End ----------------

                            if (DataElementCount != 21) {
                                //errorMessage = "File has incomplete columns";
                                errorDr["ErrorDescription"] = "File " + ExpediteFileName + " has incomplete columns in file ";
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                continue;
                            }
                            else {
                               
                                dr.ItemArray = GlobalVariable.textFileFieldValues;
                                dr["FileDate"] = Common.GetFileDate(ExpediteFileName);  //new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                dt.Rows.Add(dr);
                            }


                            if (!string.IsNullOrEmpty(errorMessage)) {
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                            }

                            TotalRecordCount += 1;
                        }
                    }
                    SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkCopy.DestinationTableName = "UP_StageSKUForecast";
                    oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                    oSqlBulkCopy.ColumnMappings.Add("OD_SKU", "OD_SKU");
                    oSqlBulkCopy.ColumnMappings.Add("Viking_SKU", "Viking_SKU");
                    oSqlBulkCopy.ColumnMappings.Add("MRP", "MRP");
                    oSqlBulkCopy.ColumnMappings.Add("PurGrp", "PurGrp");
                    oSqlBulkCopy.ColumnMappings.Add("MatGroup", "MatGroup");
                    oSqlBulkCopy.ColumnMappings.Add("StartingStock", "StartingStock");
                    oSqlBulkCopy.ColumnMappings.Add("SafetyStock", "SafetyStock");
                    oSqlBulkCopy.ColumnMappings.Add("Site", "Site");
                    oSqlBulkCopy.ColumnMappings.Add("Month1", "Month1");
                    oSqlBulkCopy.ColumnMappings.Add("Month2", "Month2");
                    oSqlBulkCopy.ColumnMappings.Add("Month3", "Month3");
                    oSqlBulkCopy.ColumnMappings.Add("Month4", "Month4");
                    oSqlBulkCopy.ColumnMappings.Add("Month5", "Month5");
                    oSqlBulkCopy.ColumnMappings.Add("Month6", "Month6");
                    oSqlBulkCopy.ColumnMappings.Add("Month7", "Month7");
                    oSqlBulkCopy.ColumnMappings.Add("Month8", "Month8");
                    oSqlBulkCopy.ColumnMappings.Add("Month9", "Month9");
                    oSqlBulkCopy.ColumnMappings.Add("Month10", "Month10");
                    oSqlBulkCopy.ColumnMappings.Add("Month11", "Month11");
                    oSqlBulkCopy.ColumnMappings.Add("Month12", "Month12");
                    oSqlBulkCopy.ColumnMappings.Add("FileDate", "FileDate");

                    oSqlBulkCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkCopy.BatchSize = dt.Rows.Count;

                    oSqlBulkCopy.WriteToServer(dt);
                    oSqlBulkCopy.Close();


                   
                    SqlBulkCopy oSqlBulkErrorCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkErrorCopy.DestinationTableName = "UP_ErrorSKUForecast";

                    oSqlBulkErrorCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                    oSqlBulkErrorCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkErrorCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");

                    oSqlBulkErrorCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkErrorCopy.BatchSize = errorDt.Rows.Count;

                    oSqlBulkErrorCopy.WriteToServer(errorDt);
                    oSqlBulkErrorCopy.Close();
                   

                }
                Common.UpdateUP_FileProcessed(fileProcessedID, TotalRecordCount, 0);
            }
            catch (Exception ex) {
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
                sbMessage.Append("Error in Expedite file:" + "\r\n" + ex.Message);
                LogUtility.WriteError(sbMessage);
            }
            finally {
                
            }
       

            try {
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spUP_ManualFileUpload";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.VarChar).Value = "InsertFiles";
                GlobalVariable.sqlComm.Parameters.Add("@DateUploaded", SqlDbType.DateTime).Value = Common.GetFileDate(ExpediteFileName);
                GlobalVariable.sqlComm.Parameters.Add("@DownloadedFilename", SqlDbType.VarChar).Value = ExpediteFileName;
                GlobalVariable.sqlComm.Parameters.Add("@UserID", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalPORecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalReceiptInformationRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalSKURecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalVendorRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalExpediteRecord", SqlDbType.Int).Value = TotalRecordCount;                
                GlobalVariable.sqlComm.Parameters.Add("@UploadType", SqlDbType.VarChar).Value = "AUTO";
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;

                GlobalVariable.sqlComm.ExecuteNonQuery();
            }
            catch (Exception ex) {
                sbMessage.Append("Error in Expedite file:" + "\r\n" + ex.Message);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            finally {
                GlobalVariable.sqlComm.CommandType = CommandType.Text;

                sbMessage.Append("No of Lines:" + TotalRecordCount);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            /// -----------------------------------------------------
        }

        private string RemoveDecimal(string val) {
            string retVal;
            retVal = RemoveSpace(val);

            if (retVal != null) {
                if (retVal.Contains(".")) {
                    retVal = retVal.Substring(0, retVal.IndexOf("."));
                }
                if (retVal.Contains("-")) {
                    retVal = '-' + (retVal.Trim('-'));
                }
                if (retVal.Contains("/")) {
                    string[] arrDate = retVal.Split('/');
                    retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }
            }
            return retVal;
        }

        private string RemoveSpace(string val) {
            string retVal;
            if (val != string.Empty)
                retVal = val.Trim(' ');
            else
                retVal = null;
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="Description"></param>
        /// <param name="ColumnLength"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public string GetErrorMessage(string ColumnName, string Description, int ColumnLength) {
            var Message = ColumnName + " length greater than " + ColumnLength + " for " + Description + ",";
            return Message;
        }

    
    }
}

﻿using System.Data.SqlClient;
using System.IO;
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using Utilities;

namespace VendorInPartnership_DataImportScheduler {
    class clsExpediteImport {

        public void UploadExpedite(string ExpediteFilePath, string ExpediteFileName) {

            DataTable dt = new DataTable();
            DataRow dr;
            int TotalRecordCount = 0;

            DataTable errorDt = new DataTable();
            DataRow errorDr;

            int Country_Index = 0;
            int OD_SKU_Index = 0;
            int Viking_SKU_Index = 0;
            int MRP_Index = 0;
            int PurGrp_Index = 0;
            int MatGroup_Index = 0;
            int StartingStock_Index = 0;
            int SafetyStock_Index = 0;
            int Site_Index = 0;
           
            int Day1_Index = 0;
            int Day2_Index = 0;
            int Day3_Index = 0;
            int Day4_Index = 0;
            int Day5_Index = 0;
            int Day6_Index = 0;
            int Day7_Index = 0;
            int Day8_Index = 0;
            int Day9_Index = 0;
            int Day10_Index = 0;
            int Day11_Index = 0;
            int Day12_Index = 0;
            int Day13_Index = 0;
            int Day14_Index = 0;
            int Day15_Index = 0;
            int Day16_Index = 0;
            int Day17_Index = 0;
            int Day18_Index = 0;
            int Day19_Index = 0;
            int Day20_Index = 0;
          

            StringBuilder sbMessage = new StringBuilder();
            int DataElementCount = 0;
       
            string FPID = Common.InsertUP_FileProcessed(GlobalVariable.folderDownloadedID.Value, TotalRecordCount, 0, ExpediteFileName);
            int fileProcessedID = Convert.ToInt32(FPID);
            try {
                using (StreamReader oStreamReader = new StreamReader(ExpediteFilePath)) {
                    GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                    var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);


                    foreach (string col in columns) {
                        switch (col.ToLower()) {
                            case "country":
                            dt.Columns.Add(new DataColumn("Country"));
                            Country_Index = Array.IndexOf(columns, "country");
                            break;
                            case "od sku":
                            dt.Columns.Add(new DataColumn("OD_SKU"));
                            OD_SKU_Index = Array.IndexOf(columns, "od sku");
                            break;
                            case "viking sku":
                            dt.Columns.Add(new DataColumn("Viking_SKU"));
                            Viking_SKU_Index = Array.IndexOf(columns, "viking sku");
                            break;
                            case "mrp":
                            dt.Columns.Add(new DataColumn("MRP"));
                            MRP_Index = Array.IndexOf(columns, "mrp");
                            break;
                            case "pur grp":
                            dt.Columns.Add(new DataColumn("PurGrp"));
                            PurGrp_Index = Array.IndexOf(columns, "pur grp");
                            break;
                            case "mat group":
                            dt.Columns.Add(new DataColumn("MatGroup"));
                            MatGroup_Index = Array.IndexOf(columns, "mat group");
                            break;
                            case "startingstock":
                            dt.Columns.Add(new DataColumn("StartingStock", typeof(System.Int32)));
                            StartingStock_Index = Array.IndexOf(columns, "startingstock");
                            break;
                            case "safety stock":
                            dt.Columns.Add(new DataColumn("SafetyStock", typeof(System.Int32)));
                            SafetyStock_Index = Array.IndexOf(columns, "safety stock");
                            break;
                            case "site":
                            dt.Columns.Add(new DataColumn("Site"));
                            Site_Index = Array.IndexOf(columns, "site");
                            break;

                            case "day1":
                            dt.Columns.Add(new DataColumn("Day1", typeof(System.Decimal)));
                            Day1_Index = Array.IndexOf(columns, "day1");
                            break;
                            case "day2":
                            dt.Columns.Add(new DataColumn("Day2", typeof(System.Decimal)));
                            Day2_Index = Array.IndexOf(columns, "day2");
                            break;
                            case "day3":
                            dt.Columns.Add(new DataColumn("Day3", typeof(System.Decimal)));
                            Day3_Index = Array.IndexOf(columns, "day3");
                            break;
                            case "day4":
                            dt.Columns.Add(new DataColumn("Day4", typeof(System.Decimal)));
                            Day4_Index = Array.IndexOf(columns, "day4");
                            break;
                            case "day5":
                            dt.Columns.Add(new DataColumn("Day5", typeof(System.Decimal)));
                            Day5_Index = Array.IndexOf(columns, "day5");
                            break;
                            case "day6":
                            dt.Columns.Add(new DataColumn("Day6", typeof(System.Decimal)));
                            Day6_Index = Array.IndexOf(columns, "day6");
                            break;
                            case "day7":
                            dt.Columns.Add(new DataColumn("Day7", typeof(System.Decimal)));
                            Day7_Index = Array.IndexOf(columns, "day7");
                            break;
                            case "day8":
                            dt.Columns.Add(new DataColumn("Day8", typeof(System.Decimal)));
                            Day8_Index = Array.IndexOf(columns, "day8");
                            break;
                            case "day9":
                            dt.Columns.Add(new DataColumn("Day9", typeof(System.Decimal)));
                            Day9_Index = Array.IndexOf(columns, "day9");
                            break;
                            case "day10":
                            dt.Columns.Add(new DataColumn("Day10", typeof(System.Decimal)));
                            Day10_Index = Array.IndexOf(columns, "day10");
                            break;
                            case "day11":
                            dt.Columns.Add(new DataColumn("Day11", typeof(System.Decimal)));
                            Day11_Index = Array.IndexOf(columns, "day11");
                            break;
                            case "day12":
                            dt.Columns.Add(new DataColumn("Day12", typeof(System.Decimal)));
                            Day12_Index = Array.IndexOf(columns, "day12");
                            break;
                            case "day13":
                            dt.Columns.Add(new DataColumn("Day13", typeof(System.Decimal)));
                            Day13_Index = Array.IndexOf(columns, "day13");
                            break;
                            case "day14":
                            dt.Columns.Add(new DataColumn("Day14", typeof(System.Decimal)));
                            Day14_Index = Array.IndexOf(columns, "day14");
                            break;
                            case "day15":
                            dt.Columns.Add(new DataColumn("Day15", typeof(System.Decimal)));
                            Day15_Index = Array.IndexOf(columns, "day15");
                            break;
                            case "day16":
                            dt.Columns.Add(new DataColumn("Day16", typeof(System.Decimal)));
                            Day16_Index = Array.IndexOf(columns, "day16");
                            break;
                            case "day17":
                            dt.Columns.Add(new DataColumn("Day17", typeof(System.Decimal)));
                            Day17_Index = Array.IndexOf(columns, "day17");
                            break;
                            case "day18":
                            dt.Columns.Add(new DataColumn("Day18", typeof(System.Decimal)));
                            Day18_Index = Array.IndexOf(columns, "day18");
                            break;
                            case "day19":
                            dt.Columns.Add(new DataColumn("Day19", typeof(System.Decimal)));
                            Day19_Index = Array.IndexOf(columns, "day19");
                            break;
                            case "day20":
                            dt.Columns.Add(new DataColumn("Day20", typeof(System.Decimal)));
                            Day20_Index = Array.IndexOf(columns, "day20");
                            break;                            
                            default:
                            break;
                        }

                    }
                    dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));

                   
                    errorDt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ManualFileID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                    errorDt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                  

                    DateTime? tempDate = DateTime.Now;

                   

                    while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null) {                     

                        string errorMessage = string.Empty;
                        errorDr = errorDt.NewRow();
                        DataElementCount = 0;

                        dr = dt.NewRow();
                        GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);                        

                        if (GlobalVariable.textFileFieldValues.Length == 29) {

                            for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++) {
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");

                            }

                            try {
                                GlobalVariable.textFileFieldValues[StartingStock_Index] = GlobalVariable.textFileFieldValues[StartingStock_Index] !=".000" ?
                                    RemoveDecimal(GlobalVariable.textFileFieldValues[StartingStock_Index]) :"0";
                                DataElementCount++;
                            }
                            catch {
                                errorMessage += columns[StartingStock_Index].ToString() + " has invalid Starting Stock - " + GlobalVariable.textFileFieldValues[StartingStock_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            try {
                                GlobalVariable.textFileFieldValues[SafetyStock_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[SafetyStock_Index]);
                                DataElementCount++;
                            }
                            catch {
                                errorMessage += columns[SafetyStock_Index].ToString() + " has invalid Safety Stock - " + GlobalVariable.textFileFieldValues[SafetyStock_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }

                            decimal temp;
                            bool IsConversionPassed = true;

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day1_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day1_Index] = GlobalVariable.textFileFieldValues[Day1_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day1_Index].ToString() + " has invalid stock on Day1 - " + GlobalVariable.textFileFieldValues[Day1_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day1_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day2_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day2_Index] = GlobalVariable.textFileFieldValues[Day2_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day2_Index].ToString() + " has invalid stock on Day2 - " + GlobalVariable.textFileFieldValues[Day2_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day2_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day3_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day3_Index] = GlobalVariable.textFileFieldValues[Day3_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day3_Index].ToString() + " has invalid stock on Day3 - " + GlobalVariable.textFileFieldValues[Day3_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day3_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day4_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day4_Index] = GlobalVariable.textFileFieldValues[Day4_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day4_Index].ToString() + " has invalid stock on Day4 - " + GlobalVariable.textFileFieldValues[Day4_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day4_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day5_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day5_Index] = GlobalVariable.textFileFieldValues[Day5_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day5_Index].ToString() + " has invalid stock on Day5 - " + GlobalVariable.textFileFieldValues[Day5_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day5_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day6_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day6_Index] = GlobalVariable.textFileFieldValues[Day6_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day6_Index].ToString() + " has invalid stock on Day6 - " + GlobalVariable.textFileFieldValues[Day6_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day6_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day7_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day7_Index] = GlobalVariable.textFileFieldValues[Day7_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day7_Index].ToString() + " has invalid stock on Day7 - " + GlobalVariable.textFileFieldValues[Day7_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day7_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day8_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day8_Index] = GlobalVariable.textFileFieldValues[Day8_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day8_Index].ToString() + " has invalid stock on Day8 - " + GlobalVariable.textFileFieldValues[Day8_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day8_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day9_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day9_Index] = GlobalVariable.textFileFieldValues[Day9_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day9_Index].ToString() + " has invalid stock on Day9 - " + GlobalVariable.textFileFieldValues[Day9_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day9_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day10_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day10_Index] = GlobalVariable.textFileFieldValues[Day10_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day10_Index].ToString() + " has invalid stock on Day10 - " + GlobalVariable.textFileFieldValues[Day10_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day10_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day11_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day11_Index] = GlobalVariable.textFileFieldValues[Day11_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day11_Index].ToString() + " has invalid stock on Day11 - " + GlobalVariable.textFileFieldValues[Day11_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day11_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day12_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day12_Index] = GlobalVariable.textFileFieldValues[Day12_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day12_Index].ToString() + " has invalid stock on Day12 - " + GlobalVariable.textFileFieldValues[Day12_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day12_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day13_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day13_Index] = GlobalVariable.textFileFieldValues[Day13_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day13_Index].ToString() + " has invalid stock on Day13 - " + GlobalVariable.textFileFieldValues[Day13_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day13_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day14_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day14_Index] = GlobalVariable.textFileFieldValues[Day14_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day14_Index].ToString() + " has invalid stock on Day14 - " + GlobalVariable.textFileFieldValues[Day14_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day14_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day15_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day15_Index] = GlobalVariable.textFileFieldValues[Day15_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                ////errorMessage += columns[Day15_Index].ToString() + " has invalid stock on Day15 - " + GlobalVariable.textFileFieldValues[Day15_Index] + " ";
                                ////errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                ////errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                ////errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                ////errorDt.Rows.Add(errorDr);
                                ////TotalRecordCount += 1;
                                ////continue;
                                GlobalVariable.textFileFieldValues[Day15_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day16_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day16_Index] = GlobalVariable.textFileFieldValues[Day16_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day16_Index].ToString() + " has invalid stock on Day16 - " + GlobalVariable.textFileFieldValues[Day16_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day16_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day17_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day17_Index] = GlobalVariable.textFileFieldValues[Day17_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day17_Index].ToString() + " has invalid stock on Day17 - " + GlobalVariable.textFileFieldValues[Day17_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day17_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day18_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day18_Index] = GlobalVariable.textFileFieldValues[Day18_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day18_Index].ToString() + " has invalid stock on Day18 - " + GlobalVariable.textFileFieldValues[Day18_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day18_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day19_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day19_Index] = GlobalVariable.textFileFieldValues[Day19_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day19_Index].ToString() + " has invalid stock on Day19 - " + GlobalVariable.textFileFieldValues[Day19_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day19_Index] = "0";
                                DataElementCount++;
                            }

                            IsConversionPassed = decimal.TryParse(GlobalVariable.textFileFieldValues[Day20_Index], out temp);
                            if (IsConversionPassed) {
                                GlobalVariable.textFileFieldValues[Day20_Index] = GlobalVariable.textFileFieldValues[Day20_Index].Trim();
                                DataElementCount++;
                            }
                            else {
                                //errorMessage += columns[Day20_Index].ToString() + " has invalid stock on Day20 - " + GlobalVariable.textFileFieldValues[Day20_Index] + " ";
                                //errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                //errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                //errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                //errorDt.Rows.Add(errorDr);
                                //TotalRecordCount += 1;
                                //continue;
                                GlobalVariable.textFileFieldValues[Day20_Index] = "0";
                                DataElementCount++;
                            }



                            //--------------------- Added on 13 Feb 2013 - Start ---------------- 
                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Country_Index])) && GlobalVariable.textFileFieldValues[Country_Index].Length > 20) {
                                errorMessage += GetErrorMessage(columns[Country_Index].ToString(),
                                                                              GlobalVariable.textFileFieldValues[Country_Index],
                                                                              20);
                                GlobalVariable.textFileFieldValues[Country_Index] = GlobalVariable.textFileFieldValues[Country_Index].Substring(0, 20);

                                DataElementCount++;
                            }
                            else {
                                DataElementCount++;
                            }


                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[OD_SKU_Index])) && GlobalVariable.textFileFieldValues[OD_SKU_Index].Length > 25) {
                                errorMessage += GetErrorMessage(columns[OD_SKU_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[OD_SKU_Index],
                                                                             25);
                                GlobalVariable.textFileFieldValues[OD_SKU_Index] = GlobalVariable.textFileFieldValues[OD_SKU_Index].Substring(0, 25);

                                DataElementCount++;
                            }
                            else {
                                DataElementCount++;
                            }

                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Viking_SKU_Index])) && GlobalVariable.textFileFieldValues[Viking_SKU_Index].Length > 25) {
                                errorMessage += GetErrorMessage(columns[Viking_SKU_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[Viking_SKU_Index],
                                                                             25);
                                GlobalVariable.textFileFieldValues[Viking_SKU_Index] = GlobalVariable.textFileFieldValues[Viking_SKU_Index].Substring(0, 25);

                                DataElementCount++;
                            }
                            else {
                                DataElementCount++;
                            }


                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[MRP_Index])) && GlobalVariable.textFileFieldValues[MRP_Index].Length > 5) {

                                errorMessage += GetErrorMessage(columns[MRP_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[MRP_Index],
                                                                             5);
                                GlobalVariable.textFileFieldValues[MRP_Index] = GlobalVariable.textFileFieldValues[MRP_Index].Substring(0, 5);

                                DataElementCount++;
                            }
                            else {
                                DataElementCount++;
                            }

                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[PurGrp_Index])) && GlobalVariable.textFileFieldValues[PurGrp_Index].Length > 5) {

                                errorMessage += GetErrorMessage(columns[PurGrp_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[PurGrp_Index],
                                                                             5);
                                GlobalVariable.textFileFieldValues[PurGrp_Index] = GlobalVariable.textFileFieldValues[PurGrp_Index].Substring(0, 5);

                                DataElementCount++;
                            }
                            else {
                                DataElementCount++;
                            }

                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[MatGroup_Index])) && GlobalVariable.textFileFieldValues[MatGroup_Index].Length > 5) {

                                errorMessage += GetErrorMessage(columns[MatGroup_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[MatGroup_Index],
                                                                             5);
                                GlobalVariable.textFileFieldValues[MatGroup_Index] = GlobalVariable.textFileFieldValues[MatGroup_Index].Substring(0, 5);

                                DataElementCount++;
                            }
                            else {
                                DataElementCount++;
                            }


                            if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Site_Index])) && GlobalVariable.textFileFieldValues[Site_Index].Length > 10) {

                                errorMessage += GetErrorMessage(columns[Site_Index].ToString(),
                                                                             GlobalVariable.textFileFieldValues[Site_Index],
                                                                             10);
                                GlobalVariable.textFileFieldValues[Site_Index] = GlobalVariable.textFileFieldValues[Site_Index].Substring(0, 10);

                                DataElementCount++;
                            }
                            else {
                                DataElementCount++;
                            }


                            //--------------------- Added on 13 Feb 2013 - End ----------------

                            if (DataElementCount != 29) {
                                //errorMessage = "File has incomplete columns";
                                errorDr["ErrorDescription"] = "File " + ExpediteFileName + " has incomplete columns in file ";
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                continue;
                            }
                            else {
                                dr.ItemArray = GlobalVariable.textFileFieldValues;
                                dr["UpdatedDate"] = Common.GetFileDate(ExpediteFileName);  //new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                dt.Rows.Add(dr);
                            }


                            if (!string.IsNullOrEmpty(errorMessage)) {
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + ExpediteFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                            }

                            TotalRecordCount += 1;
                        }
                    }
                    SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkCopy.DestinationTableName = "UP_StageSKUExpedite";

                    oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                    oSqlBulkCopy.ColumnMappings.Add("OD_SKU", "OD_SKU");
                    oSqlBulkCopy.ColumnMappings.Add("Viking_SKU", "Viking_SKU");
                    oSqlBulkCopy.ColumnMappings.Add("MRP", "MRP");
                    oSqlBulkCopy.ColumnMappings.Add("PurGrp", "PurGrp");
                    oSqlBulkCopy.ColumnMappings.Add("MatGroup", "MatGroup");
                    oSqlBulkCopy.ColumnMappings.Add("StartingStock", "StartingStock");
                    oSqlBulkCopy.ColumnMappings.Add("SafetyStock", "SafetyStock");
                    oSqlBulkCopy.ColumnMappings.Add("Site", "Site");
                    oSqlBulkCopy.ColumnMappings.Add("Day1", "Day1");
                    oSqlBulkCopy.ColumnMappings.Add("Day2", "Day2");
                    oSqlBulkCopy.ColumnMappings.Add("Day3", "Day3");
                    oSqlBulkCopy.ColumnMappings.Add("Day4", "Day4");
                    oSqlBulkCopy.ColumnMappings.Add("Day5", "Day5");
                    oSqlBulkCopy.ColumnMappings.Add("Day6", "Day6");
                    oSqlBulkCopy.ColumnMappings.Add("Day7", "Day7");
                    oSqlBulkCopy.ColumnMappings.Add("Day8", "Day8");
                    oSqlBulkCopy.ColumnMappings.Add("Day9", "Day9");
                    oSqlBulkCopy.ColumnMappings.Add("Day10", "Day10");
                    oSqlBulkCopy.ColumnMappings.Add("Day11", "Day11");
                    oSqlBulkCopy.ColumnMappings.Add("Day12", "Day12");
                    oSqlBulkCopy.ColumnMappings.Add("Day13", "Day13");
                    oSqlBulkCopy.ColumnMappings.Add("Day14", "Day14");
                    oSqlBulkCopy.ColumnMappings.Add("Day15", "Day15");
                    oSqlBulkCopy.ColumnMappings.Add("Day16", "Day16");
                    oSqlBulkCopy.ColumnMappings.Add("Day17", "Day17");
                    oSqlBulkCopy.ColumnMappings.Add("Day18", "Day18");
                    oSqlBulkCopy.ColumnMappings.Add("Day19", "Day19");
                    oSqlBulkCopy.ColumnMappings.Add("Day20", "Day20");
                   
                    oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");

                    oSqlBulkCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkCopy.BatchSize = dt.Rows.Count;

                    oSqlBulkCopy.WriteToServer(dt);
                    oSqlBulkCopy.Close();


                   
                    SqlBulkCopy oSqlBulkErrorCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkErrorCopy.DestinationTableName = "UP_ErrorSKUExpedite";

                    oSqlBulkErrorCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                    oSqlBulkErrorCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkErrorCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");

                    oSqlBulkErrorCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkErrorCopy.BatchSize = errorDt.Rows.Count;

                    oSqlBulkErrorCopy.WriteToServer(errorDt);
                    oSqlBulkErrorCopy.Close();
                   

                }
                Common.UpdateUP_FileProcessed(fileProcessedID, TotalRecordCount, 0);
            }
            catch (Exception ex) {
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
                sbMessage.Append("Error in Expedite file:" + "\r\n" + ex.Message);
                LogUtility.WriteError(sbMessage);
            }
            finally {
                
            }
       

            try {
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spUP_ManualFileUpload";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.VarChar).Value = "InsertFiles";
                GlobalVariable.sqlComm.Parameters.Add("@DateUploaded", SqlDbType.DateTime).Value = Common.GetFileDate(ExpediteFileName);
                GlobalVariable.sqlComm.Parameters.Add("@DownloadedFilename", SqlDbType.VarChar).Value = ExpediteFileName;
                GlobalVariable.sqlComm.Parameters.Add("@UserID", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalPORecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalReceiptInformationRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalSKURecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalVendorRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalExpediteRecord", SqlDbType.Int).Value = TotalRecordCount;                
                GlobalVariable.sqlComm.Parameters.Add("@UploadType", SqlDbType.VarChar).Value = "AUTO";
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;

                GlobalVariable.sqlComm.ExecuteNonQuery();
            }
            catch (Exception ex) {
                sbMessage.Append("Error in Expedite file:" + "\r\n" + ex.Message);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            finally {
                GlobalVariable.sqlComm.CommandType = CommandType.Text;

                sbMessage.Append("No of Lines:" + TotalRecordCount);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            /// -----------------------------------------------------
        }

        private string RemoveDecimal(string val) {
            string retVal;
            retVal = RemoveSpace(val);

            if (retVal != null) {
                if (retVal.Contains(".")) {
                    retVal = retVal.Substring(0, retVal.IndexOf("."));
                }
                if (retVal.Contains("-")) {
                    retVal = '-' + (retVal.Trim('-'));
                }
                if (retVal.Contains("/")) {
                    string[] arrDate = retVal.Split('/');
                    retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }
            }
            return retVal;
        }

        private string RemoveSpace(string val) {
            string retVal;
            if (val != string.Empty)
                retVal = val.Trim(' ');
            else
                retVal = null;
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="Description"></param>
        /// <param name="ColumnLength"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public string GetErrorMessage(string ColumnName, string Description, int ColumnLength) {
            var Message = ColumnName + " length greater than " + ColumnLength + " for " + Description + ",";
            return Message;
        }

    }
}





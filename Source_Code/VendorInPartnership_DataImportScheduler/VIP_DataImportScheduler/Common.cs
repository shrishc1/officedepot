﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using Utilities;

namespace VendorInPartnership_DataImportScheduler
{
    class Common
    {
        //UP_Scheduler
        public static string InsertUP_Scheduler()
        {
            StringBuilder sbMessageErr = new StringBuilder();
            string res = string.Empty;
            try {
                
                sbMessageErr.Clear();
                sbMessageErr.Append("Step 3.1.1");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);

                GlobalVariable.sqlComm.CommandText = @"INSERT INTO UP_DataImportScheduler (
                                                        StartTime,
                                                        EndTime,
                                                        CountryName,
                                                        InvokedBy,
                                                        ExecutionStatus)                                    
                                                             VALUES (getdate(), null, '" +
                                                              "','" + GlobalVariable.InvokedBy + "','Succeeded');Select @@IDENTITY;";
//////                                                               DateTime.Now.ToString() + "' , null, '" +
//////                                                              "','" + GlobalVariable.InvokedBy + "','Succeeded');Select @@IDENTITY;";

                sbMessageErr.Clear();
                sbMessageErr.Append("Step 3.1.2");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);


                 res = GlobalVariable.sqlComm.ExecuteScalar().ToString();

                sbMessageErr.Clear();
                sbMessageErr.Append(res);
                sbMessageErr.Append("\r\n");
                sbMessageErr.Append("Step 3.1.3");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);

            }
            catch (Exception ex) {
                sbMessageErr.Clear();
                sbMessageErr.Append(ex.Message + "\r\n" + ex.Source + "\r\n" + ex.InnerException);
                sbMessageErr.Append("\r\n");              
                LogUtility.SaveTraceLogEntry(sbMessageErr);
            }
            return res;

        }

        //Clear Stage Table
        public static void Initialize_Staging()
        {
            GlobalVariable.sqlComm.CommandText = "truncate table UP_StageVendor";
            GlobalVariable.sqlComm.CommandType = CommandType.Text;
            GlobalVariable.sqlComm.ExecuteNonQuery();
            GlobalVariable.sqlComm.CommandText = "truncate table UP_StageSKU";
            GlobalVariable.sqlComm.CommandType = CommandType.Text;
            GlobalVariable.sqlComm.ExecuteNonQuery();
            GlobalVariable.sqlComm.CommandText = "truncate table UP_StagePurchaseOrderDetails";
            GlobalVariable.sqlComm.CommandType = CommandType.Text;
            GlobalVariable.sqlComm.ExecuteNonQuery();
            GlobalVariable.sqlComm.CommandText = "truncate table UP_StageReceiptInformation";
            GlobalVariable.sqlComm.CommandType = CommandType.Text;
            GlobalVariable.sqlComm.ExecuteNonQuery();
            GlobalVariable.sqlComm.CommandText = "truncate table UP_StageSKUExpedite";
            GlobalVariable.sqlComm.CommandType = CommandType.Text;
            GlobalVariable.sqlComm.ExecuteNonQuery();
        }

        public static void UpdateUP_Scheduler(int SchedulerID)
        {
            StringBuilder sbMessageErr = new StringBuilder();           
            try {
                GlobalVariable.sqlComm.CommandText = "Update UP_DataImportScheduler SET EndTime = GETDATE() Where DataImportSchedulerID = " + SchedulerID;
                GlobalVariable.sqlComm.ExecuteNonQuery();
            }
            catch (Exception ex) {
                sbMessageErr.Clear();
                sbMessageErr.Append(ex.Message + "\r\n" + ex.Source + "\r\n" + ex.InnerException);
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);
            }
        }


        public static void UpdateUP_SchedulerExecutionStatus(int SchedulerID)
        {
            GlobalVariable.sqlComm.CommandText = "Update UP_DataImportScheduler SET ExecutionStatus = 'Failed' Where DataImportSchedulerID = " + SchedulerID;
            GlobalVariable.sqlComm.ExecuteNonQuery();
        }

        //Folder Downloaded
        public static string InsertUP_FolderDownload(int DataImportSchedulerID, string FolderName)
        {
            GlobalVariable.sqlComm.CommandText = @"INSERT INTO UP_FolderDownload (
                                                                        DataImportSchedulerID, 
                                                                        FolderName) 
                                                                    VALUES (" +
                                                                        DataImportSchedulerID + " , '" +
                                                                        FolderName + "');Select @@IDENTITY;";
            return GlobalVariable.sqlComm.ExecuteScalar().ToString();
        }

        public static void UpdateUP_FolderDownload(int AvailableFilesInFolder, int TotalDownloadedFile)
        {
            GlobalVariable.sqlComm.CommandText = @"Update UP_FolderDownload Set AvailableFilesInFolder = " + AvailableFilesInFolder + ", " +
                                                                        "TotalDownloadedFile = " + TotalDownloadedFile +
                                                                        "Where FolderDownloadID =" + GlobalVariable.folderDownloadedID.Value;
            GlobalVariable.sqlComm.ExecuteNonQuery();
        }
        //File Processed
        public static string InsertUP_FileProcessed(int FolderDownloadID, int NoOfRecordProcessed, int ErrorRecordCount, string FileName)
        {
            GlobalVariable.sqlComm.CommandText = "INSERT INTO UP_FileProcessed (FolderDownloadID, NoOfRecordProcessed,ErrorRecordCount,FileName) VALUES (" + FolderDownloadID + " , " + NoOfRecordProcessed + " , " + ErrorRecordCount + ",'" + FileName + "');Select @@IDENTITY;";
            return GlobalVariable.sqlComm.ExecuteScalar().ToString();
        }

        public static void UpdateUP_FileProcessed(int FileProcessedID, int NoOfRecordProcessed, int ErrorRecordCount)
        {
            GlobalVariable.sqlComm.CommandText = "Update UP_FileProcessed SET NoOfRecordProcessed = " + NoOfRecordProcessed + ",ErrorRecordCount = " + ErrorRecordCount + " Where FileProcessedID = " + FileProcessedID;
            GlobalVariable.sqlComm.ExecuteNonQuery();
        }

        //Error Log
        public static string InsertUP_ErrorLog(string ErrorMessage, int DataImportSchedulerID)
        {
            GlobalVariable.sqlComm.CommandText = "INSERT INTO UP_ErrorLog (ErrorMessage, DataImportSchedulerID) VALUES ('" + ErrorMessage + "'," + DataImportSchedulerID + " );Select @@IDENTITY;";
            return GlobalVariable.sqlComm.ExecuteScalar().ToString();
        }

        public static void UpdateLogFilePath(int FileProcessedID, string FilePath)
        {
            GlobalVariable.sqlComm.CommandText = "Update UP_FileProcessed SET FilePath = " + FilePath + " Where FileProcessedID = " + FileProcessedID;
            GlobalVariable.sqlComm.ExecuteNonQuery();
        }

        public static string CheckIsDataImportRunning()
        {
            GlobalVariable.sqlComm.CommandText = "spCheckDataImportRunning";
            GlobalVariable.sqlComm.Parameters.Clear();
            GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.NVarChar).Value = "CheckByStageTable";
            GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
            return GlobalVariable.sqlComm.ExecuteScalar().ToString();
        }

        public static string CheckImportStatus()
        {
            GlobalVariable.sqlComm.CommandText = "spCheckDataImportRunning";
            GlobalVariable.sqlComm.Parameters.Clear();
            GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.NVarChar).Value = "CheckImportStatus";
            GlobalVariable.sqlComm.Parameters.Add("@ProcessType", SqlDbType.NVarChar).Value = "A";
            GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
            return GlobalVariable.sqlComm.ExecuteScalar().ToString();
        }

        public static string UpdateImportStatus()
        {
            GlobalVariable.sqlComm.CommandText = "spCheckDataImportRunning";
            GlobalVariable.sqlComm.Parameters.Clear();
            GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.NVarChar).Value = "UpdateImportStatus";
            GlobalVariable.sqlComm.Parameters.Add("@ProcessType", SqlDbType.NVarChar).Value = "A";
            GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
            return GlobalVariable.sqlComm.ExecuteScalar().ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filename"></param>
        /// <returns></returns>
        public static DataTable GetManualFileUploadData()
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
            GlobalVariable.sqlComm.CommandText = "spUP_ManualFileUpload";
            GlobalVariable.sqlComm.Parameters.Clear();
            GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.VarChar).Value = "GetManualUploadData";
            GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(GlobalVariable.sqlComm);

            da.Fill(ds);
            dt = ds.Tables[0];
            GlobalVariable.sqlComm.CommandType = CommandType.Text;
            return dt;
        }

        /// <summary>
        /// Gets the Date of a File to be Uploaded.  
        /// </summary>
        /// <param name="Filename">string</param>
        /// <returns>DateTime</returns>
        public static DateTime GetFileDate(string Filename)
        {
            string[] File = Filename.Split('.');
            string[] strArray = File[0].Split('_');

            var Date = new DateTime(
                Convert.ToInt32(strArray[strArray.Length - 2].Substring(4, 4).ToString()),
                Convert.ToInt32(strArray[strArray.Length - 2].Substring(2, 2).ToString()),
                Convert.ToInt32(strArray[strArray.Length - 2].Substring(0, 2).ToString()),
                Convert.ToInt32(strArray[strArray.Length - 1].Substring(0, 2).ToString()),
                Convert.ToInt32(strArray[strArray.Length - 1].Substring(2, 2).ToString()),
                0);

            return Date;
        }

        /// <summary>
        /// Check To Be Uploaded File Date Is Greater
        /// </summary>
        /// <returns>bool</returns>
        public static bool CheckToBeUploadedFileDateIsGreater(string Filename)
        {
            bool Flag = false;

            bool IsAllowOldDataImport = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AllowOldDataImport"]);

            DataTable dt = Common.GetManualFileUploadData();

            string[] File = Filename.Split('.');
            string[] strArray = Filename.Split('_');

            var fileNameToCheck = File[0].Substring(0, File[0].Length - 14);


            if (strArray[strArray.Length - 2].Length < 8 || strArray[strArray.Length - 1].Length < 4)
            {
                string returnDateError = "Invalid date value. Please specify date time value in DDMMYYYY_hhmm format";
                Common.InsertUP_ErrorLog("** " + returnDateError + " in " + Filename, GlobalVariable.dataImportSchedulerID.Value);
                return false;
            }

            if (!validationFunctions.IsValidDateFormat(strArray[strArray.Length - 2]))
            {
                string returnDateError = "Invalid date format. Please specify date time value in DDMMYYYY_hhmm format";
                Common.InsertUP_ErrorLog("** " + returnDateError + " in " + Filename, GlobalVariable.dataImportSchedulerID.Value);
                return false;
            }

            var Date = new DateTime(
             Convert.ToInt32(strArray[strArray.Length - 2].Substring(4, 4).ToString()),
             Convert.ToInt32(strArray[strArray.Length - 2].Substring(2, 2).ToString()),
             Convert.ToInt32(strArray[strArray.Length - 2].Substring(0, 2).ToString()),
             Convert.ToInt32(strArray[strArray.Length - 1].Substring(0, 2).ToString()),
             Convert.ToInt32(strArray[strArray.Length - 1].Substring(2, 2).ToString()),
             0);

            List<DateTime> objList = new List<DateTime>();

            foreach (DataRow dr in dt.Rows)
            {
                if (dr["DownloadedFilename"].ToString().ToLower().Trim().Contains(fileNameToCheck.ToLower().Trim()))
                {
                    objList.Add(Convert.ToDateTime(dr["DateUploaded"]));
                }
            }

            int result = 0;
            if (objList.Count > 0)
            {
                var maxdate = DateTime.Parse(objList.Max(obj => obj).ToString());
                result = DateTime.Compare(Date, maxdate);
            }
            else
                result = 1;

            if (IsAllowOldDataImport)
                result = 1;

            if ((result > 0))
            {
                Flag = true;
            }
            else
            {
                string returnDateError = "Please upload Latest File.This file " + Filename + " is Old.";
                Common.InsertUP_ErrorLog("** " + returnDateError, GlobalVariable.dataImportSchedulerID.Value);
                return false;
            }

            return Flag;
        }



    }
}


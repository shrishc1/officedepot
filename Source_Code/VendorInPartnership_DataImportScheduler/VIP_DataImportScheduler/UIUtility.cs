﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Languages;
using BusinessLogicLayer.ModuleBAL.Languages;
using System.Reflection;

namespace VendorInPartnership_DataImportScheduler
{
    public class UIUtility
    {
        //public static List<MAS_LanguageTagsBE> lstLanguageTags = null;

        public static List<MAS_LanguageTagsBE> GetAllResources()
        {
            var lstLanguageTags = new List<MAS_LanguageTagsBE>();
            try
            {
                MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
                MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();
                if (lstLanguageTags != null && lstLanguageTags.Count <= 0)
                {
                    oMAS_LanguageTagsBE.Action = "GetAllLanguageTags";
                    lstLanguageTags = oMAS_LanguageTagsBAL.GetAllLanguageTagsBAL(oMAS_LanguageTagsBE);
                }
            }
            catch (Exception ex) { }
            return lstLanguageTags;
        }

        public static string getGlobalResourceValue(string key, string UserLanguage)
        {
            string ResourceValues = string.Empty;
            try
            {
                if (GlobalObjects.lstLanguageTags == null && GlobalObjects.lstLanguageTags.Count.Equals(0))
                    GlobalObjects.lstLanguageTags = UIUtility.GetAllResources();

                if (GlobalObjects.lstLanguageTags != null && GlobalObjects.lstLanguageTags.Count > 0)
                {
                    var lstResult = GlobalObjects.lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        Type type = lstResult[0].GetType();
                        PropertyInfo prop = type.GetProperty(UserLanguage);
                        if (prop != null)
                        {
                            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
                        }
                    }
                }
            }

            catch (Exception ex)
            {

            }
            return ResourceValues;
        }

        public static string[] GetVendorEmailsWithLanguage(int VendorID, int SiteID)
        {
            string[] VendorData = new string[2];
            MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
            APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
            string VendorEmail = string.Empty;
            List<MASSIT_VendorBE> lstVendorDetails = null;

            oMASSIT_VendorBE.Action = "GetVendorEmailForBooking"; // from user table
            oMASSIT_VendorBE.SiteVendorID = VendorID;
            oMASSIT_VendorBE.SiteID = SiteID;

            lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
            if (lstVendorDetails != null && lstVendorDetails.Count > 0)
            {

                foreach (MASSIT_VendorBE item in lstVendorDetails)
                {
                    if (item.Vendor.VendorContactEmail == null || item.Vendor.VendorContactEmail == "")
                    {
                        VendorData = GetVendorDefaultEmailWithLanguage(VendorID);
                    }
                    else
                    {
                        VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                        VendorData[1] += item.Vendor.Language.ToString() + ",";
                    }
                }
            }
            else
            {
                VendorData = GetVendorDefaultEmailWithLanguage(VendorID, SiteID);
            }
            return VendorData;
        }

        private static string[] GetVendorDefaultEmailWithLanguage(int VendorID)
        {
            string[] VendorData = new string[2];
            MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
            APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
            List<MASSIT_VendorBE> lstVendorDetails = null;
            string VendorEmail = string.Empty;
            oMASSIT_VendorBE.Action = "GetContactDetails"; // from Up_vendor table
            oMASSIT_VendorBE.SiteVendorID = VendorID;
            VendorData[0] = "";
            VendorData[1] = "";

            lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
            foreach (MASSIT_VendorBE item in lstVendorDetails)
            {
                if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
                {
                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.Language.ToString() + ",";
                }
            }
            return VendorData;
        }

        private static string[] GetVendorDefaultEmailWithLanguageID(int VendorID)
        {
            string[] VendorData = new string[2];
            MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
            APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
            List<MASSIT_VendorBE> lstVendorDetails = null;
            string VendorEmail = string.Empty;
            oMASSIT_VendorBE.Action = "GetContactDetails"; // from Up_vendor table
            oMASSIT_VendorBE.SiteVendorID = VendorID;
            VendorData[0] = "";
            VendorData[1] = "";

            lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
            foreach (MASSIT_VendorBE item in lstVendorDetails)
            {
                if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
                {
                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.LanguageID.ToString() + ",";
                }
            }
            if (!string.IsNullOrEmpty(VendorData[0]))
            {
                VendorData[0] = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
                VendorData[1] = "1";
            }

            return VendorData;
        }

        private static string[] GetVendorDefaultEmailWithLanguage(int VendorID, int pSiteID)
        {
            string[] VendorData = new string[2];
            MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
            APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
            List<MASSIT_VendorBE> lstVendorDetails = null;
            string VendorEmail = string.Empty;
            oMASSIT_VendorBE.Action = "GetContactDetailsForBooking"; // from Up_vendor table
            oMASSIT_VendorBE.SiteVendorID = VendorID;
            oMASSIT_VendorBE.SiteID = pSiteID;
            VendorData[0] = "";
            VendorData[1] = "";

            lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
            foreach (MASSIT_VendorBE item in lstVendorDetails)
            {
                if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
                {
                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.Language.ToString() + ",";
                }
            }
            if (!string.IsNullOrWhiteSpace(VendorData[0]) || VendorData[0] == "")
            {
                VendorData[0] = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
                VendorData[1] = "English";
            }
            return VendorData;
        }

        /*
         public static string getGlobalResourceValue(string key, string UserLanguage)
        {
            string ResourceValues = string.Empty;
            var lstLanguageTags = new List<MAS_LanguageTagsBE>();
            try
            {
                //Get All LanguageTags From DB
                MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
                MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();
                if (lstLanguageTags != null && lstLanguageTags.Count <= 0)
                {
                    oMAS_LanguageTagsBE.Action = "GetAllLanguageTags";
                    lstLanguageTags = oMAS_LanguageTagsBAL.GetAllLanguageTagsBAL(oMAS_LanguageTagsBE);
                    var lstResult = lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        Type type = lstResult[0].GetType();
                        PropertyInfo prop = type.GetProperty(UserLanguage);
                        if (prop != null)
                        {
                            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
                        }
                    }
                }
                else
                {

                    var lstResult = lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        Type type = lstResult[0].GetType();
                        PropertyInfo prop = type.GetProperty(UserLanguage);
                        if (prop != null)
                        {
                            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
                        }
                    }
                }
            }

            catch (Exception ex)
            {

            }
            return ResourceValues;
        }
         */
    }
}

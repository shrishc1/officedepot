﻿using System.Configuration;
using System.Data.SqlClient;
using System.Xml;

namespace VendorInPartnership_DataImportScheduler
{

    public static class GlobalVariable
    {
        public static SqlConnection sqlCon;
        public static SqlCommand sqlComm;

        public static string folderName = string.Empty;
        public static string SourceFilePath = string.Empty;
        public static string InvokedBy = string.Empty;
        public static string folderName102 = string.Empty;

        public static int? dataImportSchedulerID;
        public static int? folderDownloadedID;
        public static string TimeSpan = string.Empty;

        public static char[] delimiters = new char[] { '|' };
        public static string textFileLine = string.Empty;
        public static string[] textFileFieldValues;

        public static int CommandTimeOutTime = 10000;

        static GlobalVariable()
        {
            string ConnStr;
            ConnStr = ConfigurationManager.AppSettings["sConn"];
            sqlCon = new SqlConnection(ConnStr);
            sqlCon.Open();

            sqlComm = new SqlCommand();
            sqlComm.Connection = sqlCon;
        }
    }
}

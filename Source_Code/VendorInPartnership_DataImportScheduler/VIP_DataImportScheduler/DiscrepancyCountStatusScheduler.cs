﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System;
using System.Text;
using Utilities;

namespace VIP_DataImportScheduler
{
    public static class DiscrepancyCountStatusScheduler
    {
        public static string timeForDiscreapncy = System.Configuration.ConfigurationManager.AppSettings["timeForDiscreapncyScheduler"];

        public static void RunSchedulerForDiscrepancyCount()
        {
            var time = DateTime.Now.TimeOfDay;

            var timeForDiscreapncyScheduler = timeForDiscreapncy.Split(':');

            TimeSpan timeSpan = new TimeSpan(Convert.ToInt32(timeForDiscreapncyScheduler[0]),
                                             Convert.ToInt32(timeForDiscreapncyScheduler[1]),
                                             Convert.ToInt32(timeForDiscreapncyScheduler[2]));
            TimeSpan time1 = TimeSpan.FromHours(1);

            if (time >= timeSpan && timeSpan <= time.Add(time1))
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Started Scheduler For Open Discrepancy Count. " + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                DiscrepancyBE discrepancyReportBE = new DiscrepancyBE();
                DiscrepancyBAL discrepancyReportBAL = new DiscrepancyBAL();
                discrepancyReportBAL.GetDiscrepancyCountStatusBAL(discrepancyReportBE);

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Stop Scheduler For Open Discrepancy Count. " + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
        }
    }
}

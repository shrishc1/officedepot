﻿using System.Data.SqlClient;
using System.IO;
using System;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Data;
using System.Text;
using Utilities;

namespace VendorInPartnership_DataImportScheduler
{
    class clsVendorImport
    {
        public void UploadVendor(string vendorFilePath, string vendorFileName)
        {
            DataTable dt = new DataTable();
            DataRow dr;
            int TotalRecordCount = 0;

            //------------------------------------

            DataTable errorDt = new DataTable();
            DataRow errorDr;



            int Vendor_Country_Index = 0;
            int Vendor_No_Index = 0;
            int Vendor_Name_Index = 0;
            int Vendor_Contact_Name_Index = 0;
            int Vendor_Contact_Number_Index = 0;
            int Vendor_Contact_Fax_Index = 0;
            int Vendor_Contact_Email_Index = 0;
            int address_1_Index = 0;
            int address_2_Index = 0;
            int city_Index = 0;
            int county_Index = 0;
            int VMPPIN_Index = 0;
            int VMPPOU_Index = 0;
            int VMTCC1_Index = 0;
            int VMTEL1_Index = 0;
            int Fax_country_code_Index = 0;
            int Fax_no_Index = 0;
            int Buyer_Index = 0;

            //------------------------------------



            string FPID = Common.InsertUP_FileProcessed(GlobalVariable.folderDownloadedID.Value, TotalRecordCount, 0, vendorFileName);
            int fileProcessedID = Convert.ToInt32(FPID);
            try
            {
                using (StreamReader oStreamReader = new StreamReader(vendorFilePath, System.Text.Encoding.Default))
                {
                    GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                    var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);

                    foreach (string col in columns)
                    {

                        switch (col.ToLower())
                        {
                            case "country":
                                dt.Columns.Add(new DataColumn("Vendor_Country"));
                                Vendor_Country_Index = Array.IndexOf(columns, "country");
                                break;
                            case "vendor_no":
                                dt.Columns.Add(new DataColumn("Vendor_No"));
                                Vendor_No_Index = Array.IndexOf(columns, "vendor_no");
                                break;
                            case "vendor_name":
                                dt.Columns.Add(new DataColumn("Vendor_Name"));
                                Vendor_Name_Index = Array.IndexOf(columns, "vendor_name");
                                break;

                            case "vendor_contact_name":
                                dt.Columns.Add(new DataColumn("Vendor Contact Name"));
                                Vendor_Contact_Name_Index = Array.IndexOf(columns, "vendor_contact_name");
                                break;
                            case "vendor contact name":
                                dt.Columns.Add(new DataColumn("Vendor Contact Name"));
                                Vendor_Contact_Name_Index = Array.IndexOf(columns, "vendor contact name");
                                break;

                            case "vendor_contact_number":
                                dt.Columns.Add(new DataColumn("Vendor Contact Number"));
                                Vendor_Contact_Number_Index = Array.IndexOf(columns, "vendor_contact_number");
                                break;
                            case "vendor contact number":
                                dt.Columns.Add(new DataColumn("Vendor Contact Number"));
                                Vendor_Contact_Number_Index = Array.IndexOf(columns, "vendor contact number");
                                break;

                            case "vendor_contact_fax":
                                dt.Columns.Add(new DataColumn("Vendor Contact Fax"));
                                Vendor_Contact_Fax_Index = Array.IndexOf(columns, "vendor_contact_fax");
                                break;
                            case "vendor contact fax":
                                dt.Columns.Add(new DataColumn("Vendor Contact Fax"));
                                Vendor_Contact_Fax_Index = Array.IndexOf(columns, "vendor contact fax");
                                break;

                            case "vendor_contact_email":
                                dt.Columns.Add(new DataColumn("Vendor Contact Email"));
                                Vendor_Contact_Email_Index = Array.IndexOf(columns, "vendor_contact_email");
                                break;
                            case "vendor contact email":
                                dt.Columns.Add(new DataColumn("Vendor Contact Email"));
                                Vendor_Contact_Email_Index = Array.IndexOf(columns, "vendor contact email");
                                break;

                            case "address_1":
                                dt.Columns.Add(new DataColumn("address 1"));
                                address_1_Index = Array.IndexOf(columns, "address_1");
                                break;
                            case "address 1":
                                dt.Columns.Add(new DataColumn("address 1"));
                                address_1_Index = Array.IndexOf(columns, "address 1");
                                break;

                            case "address_2":
                                dt.Columns.Add(new DataColumn("address 2"));
                                address_2_Index = Array.IndexOf(columns, "address_2");
                                break;
                            case "address 2":
                                dt.Columns.Add(new DataColumn("address 2"));
                                address_2_Index = Array.IndexOf(columns, "address 2");
                                break;

                            case "city":
                                dt.Columns.Add(new DataColumn("city"));
                                city_Index = Array.IndexOf(columns, "city");
                                break;
                            case "county":
                                dt.Columns.Add(new DataColumn("county"));
                                county_Index = Array.IndexOf(columns, "county");
                                break;
                            case "zipcode1":
                                dt.Columns.Add(new DataColumn("VMPPIN"));
                                VMPPIN_Index = Array.IndexOf(columns, "zipcode1");
                                break;
                            case "zipcode2":
                                dt.Columns.Add(new DataColumn("VMPPOU"));
                                VMPPOU_Index = Array.IndexOf(columns, "zipcode2");
                                break;
                            case "tel1":
                                dt.Columns.Add(new DataColumn("VMTCC1"));
                                VMTCC1_Index = Array.IndexOf(columns, "tel1");
                                break;
                            case "tel2":
                                dt.Columns.Add(new DataColumn("VMTEL1"));
                                VMTEL1_Index = Array.IndexOf(columns, "tel2");
                                break;
                            case "fax_country_code":
                                dt.Columns.Add(new DataColumn("Fax_country_code"));
                                Fax_country_code_Index = Array.IndexOf(columns, "fax_country_code");
                                break;
                            case "fax_no":
                                dt.Columns.Add(new DataColumn("Fax_no"));
                                Fax_no_Index = Array.IndexOf(columns, "fax_no");
                                break;
                            case "buyer":
                                dt.Columns.Add(new DataColumn("Buyer"));
                                Buyer_Index = Array.IndexOf(columns, "buyer");
                                break;
                            default:
                                break;
                        }
                    }
                    dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));

                    //--------------------- Added on 14 Feb 2013 -----------------------
                    errorDt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ManualFileID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                    errorDt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                    //--------------------- Added on 14 Feb 2013 -----------------------

                    while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                    {
                        string errorMessage = string.Empty;
                        errorDr = errorDt.NewRow();

                        dr = dt.NewRow();
                        GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);


                        for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                        {
                            //for vendor no 5 digit
                            if (i == Vendor_No_Index)
                            {
                                var Vendor_No = GlobalVariable.textFileFieldValues[Vendor_No_Index];

                                if (GlobalVariable.textFileFieldValues[Vendor_Country_Index].ToLower() == ("UKandIRE").ToLower()
                                    && Vendor_No.Length >= 5 && Vendor_No.Substring(0, 2) == "00")
                                {
                                    Vendor_No = Vendor_No.Substring(2);
                                }

                                GlobalVariable.textFileFieldValues[i] = Vendor_No;
                            }
                           
                            GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                        }




                        //--------------------- Added on 14 Feb 2013 - Start ---------------- 

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_Country_Index])) && GlobalVariable.textFileFieldValues[Vendor_Country_Index].Length > 20)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_Country_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_Country_Index],
                                                                          20);
                            GlobalVariable.textFileFieldValues[Vendor_Country_Index] = GlobalVariable.textFileFieldValues[Vendor_Country_Index].Substring(0, 20);

                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_No_Index])) && GlobalVariable.textFileFieldValues[Vendor_No_Index].Length > 15)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_No_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_No_Index],
                                                                          15);

                            GlobalVariable.textFileFieldValues[Vendor_No_Index] = GlobalVariable.textFileFieldValues[Vendor_No_Index].Substring(0, 15);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_Name_Index])) && GlobalVariable.textFileFieldValues[Vendor_Name_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_Name_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_Name_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[Vendor_Name_Index] = GlobalVariable.textFileFieldValues[Vendor_Name_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_Contact_Name_Index])) && GlobalVariable.textFileFieldValues[Vendor_Contact_Name_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_Contact_Name_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_Contact_Name_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[Vendor_Contact_Name_Index] = GlobalVariable.textFileFieldValues[Vendor_Contact_Name_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_Contact_Number_Index])) && GlobalVariable.textFileFieldValues[Vendor_Contact_Number_Index].Length > 100)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_Contact_Number_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_Contact_Number_Index],
                                                                          100);
                            GlobalVariable.textFileFieldValues[Vendor_Contact_Number_Index] = GlobalVariable.textFileFieldValues[Vendor_Contact_Number_Index].Substring(0, 100);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_Contact_Fax_Index])) && GlobalVariable.textFileFieldValues[Vendor_Contact_Fax_Index].Length > 100)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_Contact_Fax_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_Contact_Fax_Index],
                                                                          100);
                            GlobalVariable.textFileFieldValues[Vendor_Contact_Fax_Index] = GlobalVariable.textFileFieldValues[Vendor_Contact_Fax_Index].Substring(0, 100);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_Contact_Email_Index])) && GlobalVariable.textFileFieldValues[Vendor_Contact_Email_Index].Length > 100)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_Contact_Email_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_Contact_Email_Index],
                                                                          100);
                            GlobalVariable.textFileFieldValues[Vendor_Contact_Email_Index] = GlobalVariable.textFileFieldValues[Vendor_Contact_Email_Index].Substring(0, 100);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[address_1_Index])) && GlobalVariable.textFileFieldValues[address_1_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[address_1_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[address_1_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[address_1_Index] = GlobalVariable.textFileFieldValues[address_1_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[address_2_Index])) && GlobalVariable.textFileFieldValues[address_2_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[address_2_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[address_2_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[address_2_Index] = GlobalVariable.textFileFieldValues[address_2_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[city_Index])) && GlobalVariable.textFileFieldValues[city_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[city_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[city_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[city_Index] = GlobalVariable.textFileFieldValues[city_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[county_Index])) && GlobalVariable.textFileFieldValues[county_Index].Length > 250)
                        {
                            errorMessage += GetErrorMessage(columns[county_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[county_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[county_Index] = GlobalVariable.textFileFieldValues[county_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[VMPPIN_Index])) && GlobalVariable.textFileFieldValues[VMPPIN_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[VMPPIN_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[VMPPIN_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[VMPPIN_Index] = GlobalVariable.textFileFieldValues[VMPPIN_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[VMPPOU_Index])) && GlobalVariable.textFileFieldValues[VMPPOU_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[VMPPOU_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[VMPPOU_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[VMPPOU_Index] = GlobalVariable.textFileFieldValues[VMPPOU_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[VMTCC1_Index])) && GlobalVariable.textFileFieldValues[VMTCC1_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[VMTCC1_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[VMTCC1_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[VMTCC1_Index] = GlobalVariable.textFileFieldValues[VMTCC1_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[VMTEL1_Index])) && GlobalVariable.textFileFieldValues[VMTEL1_Index].Length > 250)
                        {

                            errorMessage += GetErrorMessage(columns[VMTEL1_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[VMTEL1_Index],
                                                                          250);
                            GlobalVariable.textFileFieldValues[VMTEL1_Index] = GlobalVariable.textFileFieldValues[VMTEL1_Index].Substring(0, 250);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Fax_country_code_Index])) && GlobalVariable.textFileFieldValues[Fax_country_code_Index].Length > 5)
                        {

                            errorMessage += GetErrorMessage(columns[Fax_country_code_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Fax_country_code_Index],
                                                                          5);
                            GlobalVariable.textFileFieldValues[Fax_country_code_Index] = GlobalVariable.textFileFieldValues[Fax_country_code_Index].Substring(0, 5);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Fax_no_Index])) && GlobalVariable.textFileFieldValues[Fax_no_Index].Length > 100)
                        {

                            errorMessage += GetErrorMessage(columns[Fax_no_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Fax_no_Index],
                                                                         100);
                            GlobalVariable.textFileFieldValues[Fax_no_Index] = GlobalVariable.textFileFieldValues[Fax_no_Index].Substring(0, 100);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Buyer_Index])) && GlobalVariable.textFileFieldValues[Buyer_Index].Length > 10)
                        {

                            errorMessage += GetErrorMessage(columns[Buyer_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Buyer_Index],
                                                                          10);
                            GlobalVariable.textFileFieldValues[Buyer_Index] = GlobalVariable.textFileFieldValues[Buyer_Index].Substring(0, 10);
                        }
                        //-------------------------------------------------------------------

                        dr.ItemArray = GlobalVariable.textFileFieldValues;
                        dr["UpdatedDate"] = Common.GetFileDate(vendorFileName);  //new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                        dt.Rows.Add(dr);

                        //--------------------- Added on 14 Feb 2013 -----------------------
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + vendorFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                        }
                        //--------------------- Added on 14 Feb 2013 -----------------------


                        TotalRecordCount += 1;
                    }
                    SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkCopy.DestinationTableName = "UP_StageVendor";

                    oSqlBulkCopy.ColumnMappings.Add("Vendor_Country", "Vendor_Country");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor_No", "Vendor_No");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor_Name", "Vendor_Name");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor Contact Name", "Vendor Contact Name");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor Contact Number", "Vendor Contact Number");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor Contact Fax", "Vendor Contact Fax");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor Contact Email", "Vendor Contact Email");
                    oSqlBulkCopy.ColumnMappings.Add("address 1", "address 1");
                    oSqlBulkCopy.ColumnMappings.Add("address 2", "address 2");
                    oSqlBulkCopy.ColumnMappings.Add("city", "city");
                    oSqlBulkCopy.ColumnMappings.Add("county", "county");
                    oSqlBulkCopy.ColumnMappings.Add("VMPPIN", "VMPPIN");
                    oSqlBulkCopy.ColumnMappings.Add("VMPPOU", "VMPPOU");
                    oSqlBulkCopy.ColumnMappings.Add("VMTCC1", "VMTCC1");
                    oSqlBulkCopy.ColumnMappings.Add("VMTEL1", "VMTEL1");
                    oSqlBulkCopy.ColumnMappings.Add("Fax_country_code", "Fax_country_code");
                    oSqlBulkCopy.ColumnMappings.Add("Fax_no", "Fax_no");
                    oSqlBulkCopy.ColumnMappings.Add("Buyer", "Buyer");
                    oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");

                    oSqlBulkCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime; ;
                    oSqlBulkCopy.BatchSize = dt.Rows.Count;

                    oSqlBulkCopy.WriteToServer(dt);
                    oSqlBulkCopy.Close();

                    //--------------------- Added on 14 Feb 2013 -----------------------
                    SqlBulkCopy oSqlBulkErrorCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkErrorCopy.DestinationTableName = "UP_ErrorVendor";

                    oSqlBulkErrorCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                    oSqlBulkErrorCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkErrorCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");

                    oSqlBulkErrorCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkErrorCopy.BatchSize = errorDt.Rows.Count;

                    oSqlBulkErrorCopy.WriteToServer(errorDt);
                    oSqlBulkErrorCopy.Close();
                    //--------------------- Added on 14 Feb 2013 -----------------------

                }
                Common.UpdateUP_FileProcessed(fileProcessedID, TotalRecordCount, 0);
            }
            catch (Exception ex)
            {
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
            }
            finally
            {
                //File.Delete(vendorFilePath);
                //File.Delete(vendorFilePath + ".gz");
            }
            /// -----------------------------------------------------
            /// --- Added on 19 Feb 2013 
            /// -----------------------------------------------------
            try
            {
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spUP_ManualFileUpload";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.VarChar).Value = "InsertFiles";
                GlobalVariable.sqlComm.Parameters.Add("@DateUploaded", SqlDbType.DateTime).Value = Common.GetFileDate(vendorFileName);
                GlobalVariable.sqlComm.Parameters.Add("@DownloadedFilename", SqlDbType.VarChar).Value = vendorFileName;
                GlobalVariable.sqlComm.Parameters.Add("@UserID", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalPORecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalReceiptInformationRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalSKURecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalVendorRecord", SqlDbType.Int).Value = TotalRecordCount;
                GlobalVariable.sqlComm.Parameters.Add("@UploadType", SqlDbType.VarChar).Value = "AUTO";
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;

                GlobalVariable.sqlComm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                GlobalVariable.sqlComm.CommandType = CommandType.Text;
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("No of Lines:" + TotalRecordCount);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            /// -----------------------------------------------------
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="Description"></param>
        /// <param name="ColumnLength"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public string GetErrorMessage(string ColumnName, string Description, int ColumnLength)
        {
            var Message = ColumnName + " length greater than " + ColumnLength + " for " + Description + ",";
            return Message;
        }
    }
}

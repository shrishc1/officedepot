﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using Utilities;

namespace VendorInPartnership_DataImportScheduler {
    public static class UploadFilesToDatabase {
        public static void UploadFiles() {
            List<string> lstfileName = null;
            try {
                string vendorFilePath = string.Empty;
                string itemFilePath = string.Empty;
                string boFilePath = string.Empty;
                string boFileName = string.Empty;
                string purchaseOrderFilePath = string.Empty;
                string receiptInformationFilePath = string.Empty;
                string ExpediteFilePath = string.Empty;
                string vendorFileName = string.Empty;
                string itemFileName = string.Empty;
                string purchaseOrderFileName = string.Empty;
                string receiptInformationFileName = string.Empty;
                string ExpediteFileName = string.Empty;

                //DirectoryInfo destinationDirectory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + @"\ARCHIVE\");
                DirectoryInfo sourceDirectory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + @"\VIP_Data\");

                DirectoryInfo di = new DirectoryInfo(GlobalVariable.SourceFilePath);
                clsVendorImport objclsVendorImport = new clsVendorImport();

                StringBuilder sbMessageErr = new StringBuilder();

                #region Vendor ..
                sbMessageErr.Clear();
                sbMessageErr.Append("Step 6");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);

                #region Array concept commented.
                //lstfileName = new List<string>();
                //foreach (FileInfo fi in di.GetFiles("*.txt"))
                //{
                //    if (fi.Name.ToLower().Contains("vendor"))
                //    {
                //        lstfileName.Add(fi.Name.ToLower() + "~" + fi.FullName);
                //    }
                //}
                //string[] ArrVendorFileName = lstfileName.ToArray();
                //Array.Sort(ArrVendorFileName);

                //for (int jCount = 0; jCount < ArrVendorFileName.Length; jCount++)
                //{
                //    StringBuilder sbMessage = new StringBuilder();
                //    string[] Filei = ArrVendorFileName[jCount].ToString().Split('~');
                //    vendorFilePath = Filei[1].ToString();
                //    vendorFileName = Filei[0].ToString();

                //    sbMessage.Append("Filename:" + vendorFileName);
                //    sbMessage.Append("\r\n");
                //    sbMessage.Append("Start Time:" + DateTime.Now);
                //    sbMessage.Append("\r\n");

                //    if (Common.CheckToBeUploadedFileDateIsGreater(vendorFileName))
                //        objclsVendorImport.UploadVendor(vendorFilePath, vendorFileName);

                //    sbMessage.Append("End Time:" + DateTime.Now);
                //    sbMessage.Append("\r\n");
                //    LogUtility.SaveTraceLogEntry(sbMessage);
                //}
                #endregion

                foreach (FileInfo fi in di.GetFiles("*.txt")) {
                    StringBuilder sbMessage = new StringBuilder();
                    if (fi.Name.ToLower().Contains("vendor")) {
                        sbMessage.Append("Filename:" + fi.Name);
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Start Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");

                        vendorFilePath = fi.FullName;
                        vendorFileName = fi.Name;
                        if (Common.CheckToBeUploadedFileDateIsGreater(vendorFileName))
                            objclsVendorImport.UploadVendor(vendorFilePath, vendorFileName);

                        sbMessage.Append("End Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                }
                objclsVendorImport = null;

                sbMessageErr.Clear();
                sbMessageErr.Append("Step 7");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);
                #endregion

                #region Item ..
                clsSKUImport objclsSKUImport = new clsSKUImport();

                #region Array concept commented.
                //lstfileName = new List<string>();
                //foreach (FileInfo fi in di.GetFiles("*.txt"))
                //{
                //    if (fi.Name.ToLower().Contains("item"))
                //    {
                //        lstfileName.Add(fi.Name.ToLower() + "~" + fi.FullName);
                //    }
                //}
                //string[] ArrItemFileName = lstfileName.ToArray();
                //Array.Sort(ArrItemFileName);

                //for (int jCount = 0; jCount < ArrItemFileName.Length; jCount++)
                //{
                //    StringBuilder sbMessage = new StringBuilder();
                //    string[] Filei = ArrItemFileName[jCount].ToString().Split('~');
                //    itemFilePath = Filei[1].ToString();
                //    itemFileName = Filei[0].ToString();

                //    sbMessage.Append("Filename:" + itemFileName);
                //    sbMessage.Append("\r\n");
                //    sbMessage.Append("Start Time:" + DateTime.Now);
                //    sbMessage.Append("\r\n");

                //    if (Common.CheckToBeUploadedFileDateIsGreater(itemFileName))
                //        objclsSKUImport.UploadItem(itemFilePath, itemFileName);

                //    sbMessage.Append("End Time:" + DateTime.Now);
                //    sbMessage.Append("\r\n");
                //    LogUtility.SaveTraceLogEntry(sbMessage);
                //}
                #endregion

                foreach (FileInfo fi in di.GetFiles("*.txt")) {
                    StringBuilder sbMessage = new StringBuilder();
                    if (fi.Name.ToLower().Contains("item")) {
                        sbMessage.Append("Filename:" + fi.Name);
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Start Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");

                        itemFilePath = fi.FullName;
                        itemFileName = fi.Name;
                        if (Common.CheckToBeUploadedFileDateIsGreater(itemFileName))
                            objclsSKUImport.UploadItem(itemFilePath, itemFileName);

                        sbMessage.Append("End Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                }
                objclsSKUImport = null;

                sbMessageErr.Clear();
                sbMessageErr.Append("Step 8");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);
                #endregion

                #region NEW BO  IMPORT..

                #region Array concept commented.
                //lstfileName = new List<string>();
                //foreach (FileInfo fi in di.GetFiles("*.txt"))
                //{
                //    if (fi.Name.ToLower().Contains("item"))
                //    {
                //        lstfileName.Add(fi.Name.ToLower() + "~" + fi.FullName);
                //    }
                //}
                //string[] ArrItemFileName = lstfileName.ToArray();
                //Array.Sort(ArrItemFileName);

                //for (int jCount = 0; jCount < ArrItemFileName.Length; jCount++)
                //{
                //    StringBuilder sbMessage = new StringBuilder();
                //    string[] Filei = ArrItemFileName[jCount].ToString().Split('~');
                //    itemFilePath = Filei[1].ToString();
                //    itemFileName = Filei[0].ToString();

                //    sbMessage.Append("Filename:" + itemFileName);
                //    sbMessage.Append("\r\n");
                //    sbMessage.Append("Start Time:" + DateTime.Now);
                //    sbMessage.Append("\r\n");

                //    if (Common.CheckToBeUploadedFileDateIsGreater(itemFileName))
                //        objclsSKUImport.UploadItem(itemFilePath, itemFileName);

                //    sbMessage.Append("End Time:" + DateTime.Now);
                //    sbMessage.Append("\r\n");
                //    LogUtility.SaveTraceLogEntry(sbMessage);
                //}
                #endregion

                ClsBOImport objClsBOImport = new ClsBOImport();

                foreach (FileInfo fi in di.GetFiles("*.txt")) {
                    StringBuilder sbMessage = new StringBuilder();
                    if (fi.Name.ToLower().Contains("filbov")) {
                        sbMessage.Append("Filename:" + fi.Name);
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Start Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");

                        boFilePath = fi.FullName;
                        boFileName = fi.Name;
                        if (Common.CheckToBeUploadedFileDateIsGreater(boFileName))
                            objClsBOImport.UploadBO(boFilePath, boFileName);

                        sbMessage.Append("End Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                }
                objClsBOImport = null;

                sbMessageErr.Clear();
                sbMessageErr.Append("Step 9");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);
                #endregion

                #region Purchase Order ..
                clsPurchaseOrderImport objclsPurchaseOrderImport = new clsPurchaseOrderImport();

                #region Array concept commented.
                //lstfileName = new List<string>();
                //foreach (FileInfo fi in di.GetFiles("*.txt"))
                //{
                //    if (fi.Name.ToLower().Contains("purchaseorder"))
                //    {
                //        lstfileName.Add(fi.Name.ToLower() + "~" + fi.FullName);
                //    }
                //}
                //string[] ArrPOFileName = lstfileName.ToArray();
                //Array.Sort(ArrPOFileName);

                //for (int jCount = 0; jCount < ArrPOFileName.Length; jCount++)
                //{
                //    StringBuilder sbMessage = new StringBuilder();
                //    string[] Filei = ArrPOFileName[jCount].ToString().Split('~');
                //    purchaseOrderFilePath = Filei[1].ToString();
                //    purchaseOrderFileName = Filei[0].ToString();

                //    sbMessage.Append("Filename:" + purchaseOrderFileName);
                //    sbMessage.Append("\r\n");
                //    sbMessage.Append("Start Time:" + DateTime.Now);
                //    sbMessage.Append("\r\n");

                //    if (Common.CheckToBeUploadedFileDateIsGreater(purchaseOrderFileName))
                //        objclsPurchaseOrderImport.UploadPurchaseOrder(purchaseOrderFilePath, purchaseOrderFileName);

                //    sbMessage.Append("End Time:" + DateTime.Now);
                //    sbMessage.Append("\r\n");
                //    LogUtility.SaveTraceLogEntry(sbMessage);
                //}
                #endregion

                foreach (FileInfo fi in di.GetFiles("*.txt")) {
                    StringBuilder sbMessage = new StringBuilder();
                    if (fi.Name.ToLower().Contains("purchaseorder")) {

                        sbMessage.Append("Filename:" + fi.Name);
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Start Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");

                        purchaseOrderFilePath = fi.FullName;
                        purchaseOrderFileName = fi.Name;
                        if (Common.CheckToBeUploadedFileDateIsGreater(purchaseOrderFileName))
                            objclsPurchaseOrderImport.UploadPurchaseOrder(purchaseOrderFilePath, purchaseOrderFileName);

                        sbMessage.Append("End Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                }
                objclsPurchaseOrderImport = null;

                sbMessageErr.Clear();
                sbMessageErr.Append("Step 10");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);
                #endregion

                #region Receipt ..
                ClsReceiptInformation objclsReceiptInformation = new ClsReceiptInformation();
 
                foreach (FileInfo fi in di.GetFiles("*.txt")) {
                    StringBuilder sbMessage = new StringBuilder();
                    if (fi.Name.ToLower().Contains("receipt")) {

                        sbMessage.Append("Filename:" + fi.Name);
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Start Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");

                        receiptInformationFilePath = fi.FullName;
                        receiptInformationFileName = fi.Name;
                        if (Common.CheckToBeUploadedFileDateIsGreater(receiptInformationFileName))
                            objclsReceiptInformation.UploadReceiptInformation(receiptInformationFilePath, receiptInformationFileName);

                        sbMessage.Append("End Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                }

                objclsReceiptInformation = null;

                sbMessageErr.Clear();
                sbMessageErr.Append("Step 11");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);
                #endregion

                #region Expedite ..
                clsExpediteImport objExpediteImport = new clsExpediteImport();

                foreach (FileInfo fi in di.GetFiles("*.txt")) {
                    StringBuilder sbMessage = new StringBuilder();
                    if (fi.Name.ToLower().Contains("forecastedsales")) {

                        sbMessage.Append("Filename:" + fi.Name);
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Start Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");

                        ExpediteFilePath = fi.FullName;
                        ExpediteFileName = fi.Name;
                        if (Common.CheckToBeUploadedFileDateIsGreater(ExpediteFileName))
                            objExpediteImport.UploadExpedite(ExpediteFilePath, ExpediteFileName);

                        sbMessage.Append("End Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                }

                objExpediteImport = null;

                sbMessageErr.Clear();
                sbMessageErr.Append("Step 12");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);
                #endregion

                #region ForecastAccuracy
                clsForecastAccuracyImport objForecastImport = new clsForecastAccuracyImport();

                foreach (FileInfo fi in di.GetFiles("*.txt")) {
                    StringBuilder sbMessage = new StringBuilder();
                    if (fi.Name.ToLower().Contains("forecastaccuracy")) {

                        sbMessage.Append("Filename:" + fi.Name);
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Start Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");

                        ExpediteFilePath = fi.FullName;
                        ExpediteFileName = fi.Name;
                        if (Common.CheckToBeUploadedFileDateIsGreater(ExpediteFileName))
                            objForecastImport.UploadForecastAccuracy(ExpediteFilePath, ExpediteFileName);

                        sbMessage.Append("End Time:" + DateTime.Now);
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                }

                objForecastImport = null;

                sbMessageErr.Clear();
                sbMessageErr.Append("Step 13");
                sbMessageErr.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessageErr);
                #endregion
                ///-----------------------------------------------------
                ///---  Modified on 15 Apr 2013
                ///-----------------------------------------------------
                //UploadFilesToDatabase.ArchiveFiles(destinationDirectory, sourceDirectory);
                ///-----------------------------------------------------
                ///---  Modified on 15 Apr 2013
                ///-----------------------------------------------------

                string[] filePaths = Directory.GetFiles(GlobalVariable.SourceFilePath);
                foreach (string filePath in filePaths) {
                    File.SetAttributes(filePath, FileAttributes.Normal);
                    File.Delete(filePath);
                }
                di.Delete(true);
            }
            catch (Exception ex) {
                StringBuilder sbError = new StringBuilder();
                sbError.Append(ex.Message + "\r\n" + ex.Source + "\r\n" + ex.InnerException);
                LogUtility.SaveTraceLogEntry(sbError);
                Common.InsertUP_ErrorLog(ex.Message, GlobalVariable.dataImportSchedulerID.Value);
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
            }
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="destinationDirectory"></param>
        ///// <param name="sourceDirectory"></param>
        //public static void ArchiveFiles(DirectoryInfo destinationDirectory, DirectoryInfo sourceDirectory)
        //{
        //    try
        //    {
        //        if (Directory.Exists(sourceDirectory.FullName))
        //        {
        //            string[] subFolders = Directory.GetDirectories(sourceDirectory.FullName);
        //            foreach (string subSourceFolder in subFolders)
        //            {
        //                string FolderName = Path.GetFileName(subSourceFolder);
        //                string DestinationFolderName = Path.Combine(destinationDirectory.FullName, FolderName);
        //                if (Directory.Exists(DestinationFolderName))
        //                {
        //                    string[] subDirectories = Directory.GetDirectories(DestinationFolderName);
        //                    string[] files = Directory.GetFiles(subSourceFolder);
        //                    int count = subDirectories.Count();
        //                    string SubFolderName = Path.Combine(DestinationFolderName, (count + 1).ToString());
        //                    if (!Directory.Exists(SubFolderName))
        //                        Directory.CreateDirectory(SubFolderName);

        //                    foreach (string sourceFile in files)
        //                    {
        //                        string fileName = Path.GetFileName(sourceFile);
        //                        string destFile = Path.Combine(SubFolderName, fileName);
        //                        File.Move(sourceFile, destFile);
        //                    }
        //                }
        //                else
        //                {
        //                    string[] files = Directory.GetFiles(subSourceFolder);
        //                    string SubFolderName = Path.Combine(DestinationFolderName, "1");
        //                    if (!Directory.Exists(SubFolderName))
        //                        Directory.CreateDirectory(SubFolderName);

        //                    foreach (string sourceFile in files)
        //                    {
        //                        string fileName = Path.GetFileName(sourceFile);
        //                        string destFile = Path.Combine(SubFolderName, fileName);
        //                        File.Move(sourceFile, destFile);
        //                    }
        //                }
        //                Directory.Delete(subSourceFolder);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Common.InsertUP_ErrorLog(ex.Message, GlobalVariable.dataImportSchedulerID.Value);
        //    }
        //}

    }
}

﻿using System.Data.SqlClient;
using System.IO;
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using Utilities;

namespace VendorInPartnership_DataImportScheduler
{
    class clsPurchaseOrderImport
    {

        public void UploadPurchaseOrder(string purchaseOrderFilePath, string purchaseOrderFileName)
        {

            DataTable dt = new DataTable();
            DataRow dr;
            int TotalRecordCount = 0;

            int Line_NoIndex = 0;
            int Original_quantityIndex = 0;
            int Outstanding_QtyIndex = 0;
            int PO_costIndex = 0;
            int Order_raisedIndex = 0;
            int Original_due_dateIndex = 0;
            int Expected_dateIndex = 0;
            int subvndr_Index = 0;
            int Snooty_Index = 0;

            bool IsSnooty = false;
            //--------------------- Added on 13 Feb 2013 - Start ---------------- 

            DataTable errorDt = new DataTable();
            DataRow errorDr;

            DateTime FileDate = new DateTime();
            FileDate = GetFileDate(purchaseOrderFileName);

            int Purchase_order_Index = 0;
            int Warehouse_Index = 0;
            int Vendor_No_Index = 0;
            int Direct_code_Index = 0;
            int OD_Code_Index = 0;
            int Vendor_Code_Index = 0;
            int Product_description_Index = 0;
            int UOM_Index = 0;
            int Buyer_no_Index = 0;
            int Item_classification_Index = 0;
            int Item_type_Index = 0;
            int Item_Category_Index = 0;
            int Other_Index = 0;
            int Country_Index = 0;
            StringBuilder sbMessage = new StringBuilder();
            int DataElementCount = 0;
            //--------------------- Added on 13 Feb 2013 - End ---------------- 

            string FPID = Common.InsertUP_FileProcessed(GlobalVariable.folderDownloadedID.Value, TotalRecordCount, 0, purchaseOrderFileName);
            int fileProcessedID = Convert.ToInt32(FPID);
            try
            {
                using (StreamReader oStreamReader = new StreamReader(purchaseOrderFilePath))
                {
                    GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                    var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);

                    foreach (string col in columns)
                    {
                        switch (col.ToLower())
                        {
                            case "purchase_order":
                                dt.Columns.Add(new DataColumn("Purchase_order"));
                                Purchase_order_Index = Array.IndexOf(columns, "purchase_order");
                                break;
                            case "line_no":
                                dt.Columns.Add(new DataColumn("Line_No", typeof(System.Int32)));
                                Line_NoIndex = Array.IndexOf(columns, "line_no");
                                break;
                            case "warehouse":
                                dt.Columns.Add(new DataColumn("Warehouse"));
                                Warehouse_Index = Array.IndexOf(columns, "warehouse");
                                break;
                            case "vendor_no":
                                dt.Columns.Add(new DataColumn("Vendor_No"));
                                Vendor_No_Index = Array.IndexOf(columns, "vendor_no");
                                break;
                            case "subvndr":
                                dt.Columns.Add(new DataColumn("subvndr"));
                                subvndr_Index = Array.IndexOf(columns, "subvndr");
                                break;
                            case "direct_code":
                                dt.Columns.Add(new DataColumn("Direct_code"));
                                Direct_code_Index = Array.IndexOf(columns, "direct_code");
                                break;
                            case "od_code":
                                dt.Columns.Add(new DataColumn("OD_Code"));
                                OD_Code_Index = Array.IndexOf(columns, "od_code");
                                break;
                            case "vendor_code":
                                dt.Columns.Add(new DataColumn("Vendor_Code"));
                                Vendor_Code_Index = Array.IndexOf(columns, "vendor_code");
                                break;
                            case "product_description":
                                dt.Columns.Add(new DataColumn("Product_description"));
                                Product_description_Index = Array.IndexOf(columns, "product_description");
                                break;
                            case "uom":
                                dt.Columns.Add(new DataColumn("UOM"));
                                UOM_Index = Array.IndexOf(columns, "uom");
                                break;
                            case "original_quantity":
                                dt.Columns.Add(new DataColumn("Original_quantity", typeof(System.Int32)));
                                Original_quantityIndex = Array.IndexOf(columns, "original_quantity");
                                break;
                            case "outstanding_qty":
                                dt.Columns.Add(new DataColumn("Outstanding_Qty", typeof(System.Int32)));
                                Outstanding_QtyIndex = Array.IndexOf(columns, "outstanding_qty");
                                break;
                            case "po_cost":
                                dt.Columns.Add(new DataColumn("PO_cost", typeof(System.Decimal)));
                                PO_costIndex = Array.IndexOf(columns, "po_cost");
                                break;
                            case "order_raised":
                                dt.Columns.Add(new DataColumn("Order_raised", typeof(System.DateTime)));
                                Order_raisedIndex = Array.IndexOf(columns, "order_raised");
                                break;
                            case "original_due_date":
                                dt.Columns.Add(new DataColumn("Original_due_date", typeof(System.DateTime)));
                                Original_due_dateIndex = Array.IndexOf(columns, "original_due_date");
                                break;
                            case "expected_date":
                                dt.Columns.Add(new DataColumn("Expected_date", typeof(System.DateTime)));
                                Expected_dateIndex = Array.IndexOf(columns, "expected_date");
                                break;
                            case "buyer_no":
                                dt.Columns.Add(new DataColumn("Buyer_no"));
                                Buyer_no_Index = Array.IndexOf(columns, "buyer_no");

                                break;
                            case "item_classification":
                                dt.Columns.Add(new DataColumn("Item_classification"));
                                Item_classification_Index = Array.IndexOf(columns, "item_classification");

                                break;
                            case "item_type":
                                dt.Columns.Add(new DataColumn("Item_type"));
                                Item_type_Index = Array.IndexOf(columns, "item_type");     // Added by Hemant on 13 Feb 2013

                                break;
                            case "item_category":
                                dt.Columns.Add(new DataColumn("Item_Category"));
                                Item_Category_Index = Array.IndexOf(columns, "item_category");  // Added by Hemant on 13 Feb 2013

                                break;
                            case "other":
                                dt.Columns.Add(new DataColumn("Other"));
                                Other_Index = Array.IndexOf(columns, "other");   // Added by Hemant on 13 Feb 2013

                                break;
                            case "country":
                                dt.Columns.Add(new DataColumn("Country"));
                                Country_Index = Array.IndexOf(columns, "country");// Added by Hemant on 13 Feb 2013
                                break;
                            case "snooty":
                                dt.Columns.Add(new DataColumn("Snooty"));
                                Snooty_Index = Array.IndexOf(columns, "snooty");// Added by shrish on 25 nov 2019
                                break;
                            default:
                                break;
                        }
                    }
                    dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));

                    //--------------------- Added on 14 Feb 2013 -----------------------
                    errorDt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ManualFileID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                    errorDt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                    //--------------------- Added on 14 Feb 2013 -----------------------

                    DateTime? tempDate = DateTime.Now;

                    while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                    {
                        string errorMessage = string.Empty;
                        errorDr = errorDt.NewRow();
                        DataElementCount = 0;

                        dr = dt.NewRow();
                        GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);

                        if (GlobalVariable.textFileFieldValues.Length == 23)
                            IsSnooty = true;
                        else
                            IsSnooty = false;

                        if (GlobalVariable.textFileFieldValues.Length == 23 && IsSnooty)
                        {
                            for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                            {
                                try
                                {
                                    //for vendor no 5 digit
                                    if (i == Vendor_No_Index)
                                    {
                                        var Vendor_No = GlobalVariable.textFileFieldValues[Vendor_No_Index];

                                        if (GlobalVariable.textFileFieldValues[Country_Index].ToLower() == ("UKandIRE").ToLower()
                                            && Vendor_No.Length >= 5 && Vendor_No.Substring(0, 2) == "00")
                                        {
                                            Vendor_No = Vendor_No.Substring(2);
                                        }

                                        GlobalVariable.textFileFieldValues[i] = Vendor_No;
                                    }

                                    GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }
                        else if(GlobalVariable.textFileFieldValues.Length == 22)
                        {
                            for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                            {
                                try
                                {
                                    //for vendor no 5 digit
                                    if (i == Vendor_No_Index)
                                    {
                                        var Vendor_No = GlobalVariable.textFileFieldValues[Vendor_No_Index];

                                        if (GlobalVariable.textFileFieldValues[Country_Index].ToLower() == ("UKandIRE").ToLower()
                                            && Vendor_No.Length >= 5 && Vendor_No.Substring(0, 2) == "00")
                                        {
                                            Vendor_No = Vendor_No.Substring(2);
                                        }

                                        GlobalVariable.textFileFieldValues[i] = Vendor_No;
                                    }

                                    GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            continue;
                        }

                        //int columnCounter = 0;
                        //while(columnCounter <= (dt.Columns.Count - 1))
                        //{
                        //    if (dt.Columns[columnCounter].GetType().ToString() == "Int32")
                        //        GlobalVariable.textFileFieldValues[columnCounter] = RemoveDecimal(GlobalVariable.textFileFieldValues[columnCounter]);
                        //    if (dt.Columns[columnCounter].GetType().ToString() == "Decimal")
                        //        if (GlobalVariable.textFileFieldValues[columnCounter] == string.Empty)
                        //            GlobalVariable.textFileFieldValues[columnCounter] = null;


                        //    columnCounter++;
                        //}



                        try {
                            GlobalVariable.textFileFieldValues[Line_NoIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Line_NoIndex]);
                            DataElementCount++;
                        }
                        catch {
                            errorMessage += columns[Line_NoIndex].ToString() + " has invalid Line No - " + GlobalVariable.textFileFieldValues[Line_NoIndex] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }

                        try {
                            GlobalVariable.textFileFieldValues[Original_quantityIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Original_quantityIndex]);
                            DataElementCount++;
                        }
                        catch {
                            errorMessage += columns[Original_quantityIndex].ToString() + " has invalid Original quantity - " + GlobalVariable.textFileFieldValues[Original_quantityIndex] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }

                        try {
                            GlobalVariable.textFileFieldValues[Outstanding_QtyIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Outstanding_QtyIndex]);
                            DataElementCount++;
                        }
                        catch {
                            errorMessage += columns[Outstanding_QtyIndex].ToString() + " has invalid Outstanding Qty - " + GlobalVariable.textFileFieldValues[Outstanding_QtyIndex] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }

                        //Add by Abhinav
                        try {
                            tempDate = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[Order_raisedIndex]));
                            GlobalVariable.textFileFieldValues[Order_raisedIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Order_raisedIndex]);
                            DataElementCount++;
                        }
                        catch {
                            errorMessage += columns[Order_raisedIndex].ToString() + " has invalid date - " + GlobalVariable.textFileFieldValues[Order_raisedIndex] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }

                        try {
                            tempDate = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[Original_due_dateIndex]));
                            GlobalVariable.textFileFieldValues[Original_due_dateIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Original_due_dateIndex]);
                            DataElementCount++;
                        }
                        catch {
                            errorMessage += columns[Original_due_dateIndex].ToString() + " has invalid date - " + GlobalVariable.textFileFieldValues[Original_due_dateIndex] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }

                        try {
                            tempDate = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[Expected_dateIndex]));
                            GlobalVariable.textFileFieldValues[Expected_dateIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Expected_dateIndex]);
                            DataElementCount++;
                        }
                        catch {
                            errorMessage += columns[Expected_dateIndex].ToString() + " has invalid date - " + GlobalVariable.textFileFieldValues[Expected_dateIndex] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }

                        //GlobalVariable.textFileFieldValues[Order_raisedIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Order_raisedIndex]);
                        //GlobalVariable.textFileFieldValues[Original_due_dateIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Original_due_dateIndex]);
                        //GlobalVariable.textFileFieldValues[Expected_dateIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Expected_dateIndex]);
                        //---------------------------//

                        try {
                            GlobalVariable.textFileFieldValues[PO_costIndex] = RemoveSpace(GlobalVariable.textFileFieldValues[PO_costIndex]);
                            DataElementCount++;
                        }
                        catch {
                            errorMessage += columns[PO_costIndex].ToString() + " has invalid PO cost - " + GlobalVariable.textFileFieldValues[PO_costIndex] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }

                        //--------------------- Added on 13 Feb 2013 - Start ---------------- 
                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Purchase_order_Index])) && GlobalVariable.textFileFieldValues[Purchase_order_Index].Length > 15) {
                            errorMessage += GetErrorMessage(columns[Purchase_order_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Purchase_order_Index],
                                                                          15);
                            GlobalVariable.textFileFieldValues[Purchase_order_Index] = GlobalVariable.textFileFieldValues[Purchase_order_Index].Substring(0, 15);

                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }
                        

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Warehouse_Index])) && GlobalVariable.textFileFieldValues[Warehouse_Index].Length > 10)
                        {
                            errorMessage += GetErrorMessage(columns[Warehouse_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Warehouse_Index],
                                                                         10);
                            GlobalVariable.textFileFieldValues[Warehouse_Index] = GlobalVariable.textFileFieldValues[Warehouse_Index].Substring(0, 10);
                       
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_No_Index])) && GlobalVariable.textFileFieldValues[Vendor_No_Index].Length > 15)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_No_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Vendor_No_Index],
                                                                         15);
                            GlobalVariable.textFileFieldValues[Vendor_No_Index] = GlobalVariable.textFileFieldValues[Vendor_No_Index].Substring(0, 15);
                       
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }


                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Direct_code_Index])) && GlobalVariable.textFileFieldValues[Direct_code_Index].Length > 25)
                        {

                            errorMessage += GetErrorMessage(columns[Direct_code_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Direct_code_Index],
                                                                         25);
                            GlobalVariable.textFileFieldValues[Direct_code_Index] = GlobalVariable.textFileFieldValues[Direct_code_Index].Substring(0, 25);
                        
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[OD_Code_Index])) && GlobalVariable.textFileFieldValues[OD_Code_Index].Length > 25)
                        {

                            errorMessage += GetErrorMessage(columns[OD_Code_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[OD_Code_Index],
                                                                         25);
                            GlobalVariable.textFileFieldValues[OD_Code_Index] = GlobalVariable.textFileFieldValues[OD_Code_Index].Substring(0, 25);

                       
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_Code_Index])) && GlobalVariable.textFileFieldValues[Vendor_Code_Index].Length > 50)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_Code_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Vendor_Code_Index],
                                                                         50);
                            GlobalVariable.textFileFieldValues[Vendor_Code_Index] = GlobalVariable.textFileFieldValues[Vendor_Code_Index].Substring(0, 50);
                       
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Product_description_Index])) && GlobalVariable.textFileFieldValues[Product_description_Index].Length > 50)
                        {

                            errorMessage += GetErrorMessage(columns[Product_description_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Product_description_Index],
                                                                         50);
                            GlobalVariable.textFileFieldValues[Product_description_Index] = GlobalVariable.textFileFieldValues[Product_description_Index].Substring(0, 50);
                        
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[UOM_Index])) && GlobalVariable.textFileFieldValues[UOM_Index].Length > 5)
                        {

                            errorMessage += GetErrorMessage(columns[UOM_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[UOM_Index],
                                                                         5);
                            GlobalVariable.textFileFieldValues[UOM_Index] = GlobalVariable.textFileFieldValues[UOM_Index].Substring(0, 5);
                        
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Buyer_no_Index])) && GlobalVariable.textFileFieldValues[Buyer_no_Index].Length > 20)
                        {

                            errorMessage += GetErrorMessage(columns[Buyer_no_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Buyer_no_Index],
                                                                         20);
                            GlobalVariable.textFileFieldValues[Buyer_no_Index] = GlobalVariable.textFileFieldValues[Buyer_no_Index].Substring(0, 20);

                        
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Item_classification_Index])) && GlobalVariable.textFileFieldValues[Item_classification_Index].Length > 5)
                        {

                            errorMessage += GetErrorMessage(columns[Item_classification_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Item_classification_Index],
                                                                         5);
                            GlobalVariable.textFileFieldValues[Item_classification_Index] = GlobalVariable.textFileFieldValues[Item_classification_Index].Substring(0, 5);

                      
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Item_type_Index])) && GlobalVariable.textFileFieldValues[Item_type_Index].Length > 10)
                        {

                            errorMessage += GetErrorMessage(columns[Item_type_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Item_type_Index],
                                                                         10);
                            GlobalVariable.textFileFieldValues[Item_type_Index] = GlobalVariable.textFileFieldValues[Item_type_Index].Substring(0, 10);

                       
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Item_Category_Index])) && GlobalVariable.textFileFieldValues[Item_Category_Index].Length > 10)
                        {

                            errorMessage += GetErrorMessage(columns[Item_Category_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Item_Category_Index],
                                                                         10);
                            GlobalVariable.textFileFieldValues[Item_Category_Index] = GlobalVariable.textFileFieldValues[Item_Category_Index].Substring(0, 10);

                       
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Other_Index])) && GlobalVariable.textFileFieldValues[Other_Index].Length > 500)
                        {

                            errorMessage += GetErrorMessage(columns[Other_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Other_Index],
                                                                         500);
                            GlobalVariable.textFileFieldValues[Other_Index] = GlobalVariable.textFileFieldValues[Other_Index].Substring(0, 500);
                       
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Country_Index])) && GlobalVariable.textFileFieldValues[Country_Index].Length > 20)
                        {

                            errorMessage += GetErrorMessage(columns[Country_Index].ToString(),
                                                                         GlobalVariable.textFileFieldValues[Country_Index],
                                                                         20);
                            GlobalVariable.textFileFieldValues[Country_Index] = GlobalVariable.textFileFieldValues[Country_Index].Substring(0, 20);

                        
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }

                        /*-----------------------------------------------------------------------------------------------------*/
                        if (IsSnooty)
                        {
                            try
                            {
                                GlobalVariable.textFileFieldValues[Snooty_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[Snooty_Index]);
                                DataElementCount++;
                            }
                            catch
                            {
                                errorMessage += columns[Snooty_Index].ToString() + " has invalid Outstanding Qty - " + GlobalVariable.textFileFieldValues[Snooty_Index] + " ";
                                errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                TotalRecordCount += 1;
                                continue;
                            }
                        }
                        /*-----------------------------------------------------------------------------------------------------*/


                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[subvndr_Index])) && GlobalVariable.textFileFieldValues[subvndr_Index].Length > 10)
                        {

                            errorMessage += GetErrorMessage(columns[subvndr_Index].ToString(), GlobalVariable.textFileFieldValues[subvndr_Index],
                                                                         10);

                            GlobalVariable.textFileFieldValues[subvndr_Index] = GlobalVariable.textFileFieldValues[subvndr_Index].Substring(0, 10);
                       
                            DataElementCount++;
                        }
                        else {
                            DataElementCount++;
                        }
                        //--------------------- Added on 13 Feb 2013 - End ----------------
                        if (IsSnooty)
                        {
                            if (DataElementCount != 23)
                            {
                                //errorMessage = "File has incomplete columns";
                                errorDr["ErrorDescription"] = "File " + purchaseOrderFileName + " has incomplete columns in file ";
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                continue;
                            }
                            else
                            {
                                dr.ItemArray = GlobalVariable.textFileFieldValues;
                                dr["UpdatedDate"] = FileDate; // new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                dt.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            if (DataElementCount != 22)
                            {
                                //errorMessage = "File has incomplete columns";
                                errorDr["ErrorDescription"] = "File " + purchaseOrderFileName + " has incomplete columns in file ";
                                errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                                errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                errorDt.Rows.Add(errorDr);
                                continue;
                            }
                            else
                            {
                                dr.ItemArray = GlobalVariable.textFileFieldValues;
                                dr["UpdatedDate"] = FileDate; // new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                                dt.Rows.Add(dr);
                            }
                        }
                        //--------------------- Added on 14 Feb 2013 -----------------------
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                        }
                        //--------------------- Added on 14 Feb 2013 -----------------------

                        TotalRecordCount += 1;
                    }

                    SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkCopy.DestinationTableName = "UP_StagePurchaseOrderDetails";
                    oSqlBulkCopy.ColumnMappings.Add("Purchase_order", "Purchase_order");
                    oSqlBulkCopy.ColumnMappings.Add("Line_No", "Line_No");
                    oSqlBulkCopy.ColumnMappings.Add("Warehouse", "Warehouse");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor_No", "Vendor_No");
                    oSqlBulkCopy.ColumnMappings.Add("subvndr", "subvndr");
                    oSqlBulkCopy.ColumnMappings.Add("Direct_code", "Direct_code");
                    oSqlBulkCopy.ColumnMappings.Add("OD_Code", "OD_Code");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor_Code", "Vendor_Code");
                    oSqlBulkCopy.ColumnMappings.Add("Product_description", "Product_description");
                    oSqlBulkCopy.ColumnMappings.Add("UOM", "UOM");
                    oSqlBulkCopy.ColumnMappings.Add("Original_quantity", "Original_quantity");
                    oSqlBulkCopy.ColumnMappings.Add("Outstanding_Qty", "Outstanding_Qty");
                    oSqlBulkCopy.ColumnMappings.Add("PO_cost", "PO_cost");
                    oSqlBulkCopy.ColumnMappings.Add("Order_raised", "Order_raised");
                    oSqlBulkCopy.ColumnMappings.Add("Original_due_date", "Original_due_date");
                    oSqlBulkCopy.ColumnMappings.Add("Expected_date", "Expected_date");
                    oSqlBulkCopy.ColumnMappings.Add("Buyer_no", "Buyer_no");
                    oSqlBulkCopy.ColumnMappings.Add("Item_classification", "Item_classification");
                    oSqlBulkCopy.ColumnMappings.Add("Item_type", "Item_type");
                    oSqlBulkCopy.ColumnMappings.Add("Item_Category", "Item_Category");
                    oSqlBulkCopy.ColumnMappings.Add("Other", "Other");
                    oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                    if (IsSnooty)
                    {
                        oSqlBulkCopy.ColumnMappings.Add("Snooty", "Snooty");
                    }
                    oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");

                    oSqlBulkCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkCopy.BatchSize = dt.Rows.Count;

                    oSqlBulkCopy.WriteToServer(dt);
                    oSqlBulkCopy.Close();


                    //--------------------- Added on 14 Feb 2013 -----------------------
                    SqlBulkCopy oSqlBulkErrorCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkErrorCopy.DestinationTableName = "UP_ErrorPurchaseOrderDetails";

                    oSqlBulkErrorCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                    oSqlBulkErrorCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkErrorCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");

                    oSqlBulkErrorCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkErrorCopy.BatchSize = errorDt.Rows.Count;

                    oSqlBulkErrorCopy.WriteToServer(errorDt);
                    oSqlBulkErrorCopy.Close();
                    //--------------------- Added on 14 Feb 2013 -----------------------

                }
                Common.UpdateUP_FileProcessed(fileProcessedID, TotalRecordCount, 0);
            }
            catch (Exception ex)
            {
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
                sbMessage.Append("Error in purchase order file:" + "\r\n" + ex.Message);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            finally
            {
                //File.Delete(purchaseOrderFilePath);
                //File.Delete(purchaseOrderFilePath + ".gz");
            }

            /// -----------------------------------------------------
            /// --- Added on 19 Feb 2013 
            /// -----------------------------------------------------
            
            try {
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spUP_ManualFileUpload";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.VarChar).Value = "InsertFiles";
                GlobalVariable.sqlComm.Parameters.Add("@DateUploaded", SqlDbType.DateTime).Value = Common.GetFileDate(purchaseOrderFileName);
                GlobalVariable.sqlComm.Parameters.Add("@DownloadedFilename", SqlDbType.VarChar).Value = purchaseOrderFileName;
                GlobalVariable.sqlComm.Parameters.Add("@UserID", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalPORecord", SqlDbType.Int).Value = TotalRecordCount;
                GlobalVariable.sqlComm.Parameters.Add("@TotalReceiptInformationRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalSKURecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalVendorRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@UploadType", SqlDbType.VarChar).Value = "AUTO";
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;

                GlobalVariable.sqlComm.ExecuteNonQuery();
            }
            catch (Exception ex) {
                sbMessage.Append("Error in purchase order file:" + "\r\n" + ex.Message);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            finally { 
                GlobalVariable.sqlComm.CommandType = CommandType.Text;
               
                sbMessage.Append("No of Lines:" + TotalRecordCount);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            /// -----------------------------------------------------
        }

        /// <summary>
        /// Gets the Date of a File to be Uploaded.  
        /// </summary>
        /// <param name="Filename">string</param>
        /// <returns>DateTime</returns>
        public DateTime GetFileDate(string Filename) {
            string[] File = Filename.Split('.');
            string[] strArray = File[0].Split('_');

            var Date = new DateTime(
                Convert.ToInt32(strArray[strArray.Length - 2].Substring(4, 4).ToString()),
                Convert.ToInt32(strArray[strArray.Length - 2].Substring(2, 2).ToString()),
                Convert.ToInt32(strArray[strArray.Length - 2].Substring(0, 2).ToString()),
                Convert.ToInt32(strArray[strArray.Length - 1].Substring(0, 2).ToString()),
                Convert.ToInt32(strArray[strArray.Length - 1].Substring(2, 2).ToString()),
                0);

            return Date;
        }

        private string RemoveDecimal(string val)
        {
            string retVal;
            retVal = RemoveSpace(val);

            if (retVal != null)
            {
                if (retVal.Contains("."))
                {
                    retVal = retVal.Substring(0, retVal.IndexOf("."));
                }
                if (retVal.Contains("-"))
                {
                    retVal = '-' + (retVal.Trim('-'));
                }
                if (retVal.Contains("/"))
                {
                    string[] arrDate = retVal.Split('/');
                    retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }
            }
            return retVal;
        }

        private string RemoveSpace(string val)
        {
            string retVal;
            if (val != string.Empty)
                retVal = val.Trim(' ');
            else
                retVal = null;
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="Description"></param>
        /// <param name="ColumnLength"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public string GetErrorMessage(string ColumnName, string Description, int ColumnLength)
        {
            var Message = ColumnName + " length greater than " + ColumnLength + " for " + Description + ",";
            return Message;
        }

    }
}

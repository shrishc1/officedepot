﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogicLayer.ModuleBAL.StockOverview.Report;
using BusinessEntities.ModuleBE.StockOverview.Report;
using System.Data;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using Microsoft.Reporting.WinForms;
using BusinessLogicLayer.ModuleBAL.StockOverview.StockTurn;
using BusinessEntities.ModuleBE.StockOverview.StockTurn;
using System.IO;
using Utilities;

namespace VendorSTScheduler.ModuleUI.StockTurn
{
    public class GenerateReport
    {
        private DataRow dr;

        DataSet dsStockOverview = new DataSet();

        #region ReportVariables

        Warning[] warnings;
        string[] streamIds;
        string mimeType = string.Empty;
        string encoding = string.Empty;
        string extension = string.Empty;
        string reportFileName = string.Empty;

        #endregion

        public void StartReportGeneration(DateTime dtDateFrom, DateTime dtDateTo)
        {          
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
            UP_VendorBE oUP_VendorBE = new UP_VendorBE();       
            try
            {               
                StockTurnReportBE oStockTurnReportBE = new StockTurnReportBE();
                StockTurnReportBAL oStockTurnReportBAL = new StockTurnReportBAL();
                oStockTurnReportBE.ReportType = "Detailed_View_Vendor";
                oStockTurnReportBE.DateFrom = dtDateFrom;
                oStockTurnReportBE.DateTo = dtDateTo;
                oStockTurnReportBE.IsVendorScheduler = true;
                oStockTurnReportBAL.getStockTurnODViewReportBAL(oStockTurnReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
        }
    }
}
              
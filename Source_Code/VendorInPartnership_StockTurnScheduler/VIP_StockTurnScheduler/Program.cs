﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using VendorSTScheduler.ModuleUI.StockTurn;
using Utilities;
using System.Text;

namespace VendorSTScheduler
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]

        static string connString = @"Data Source=172.29.9.4;Initial Catalog=OD_VIP_Stage27_R1;
         Persist Security Info=True;User ID=officedepot;Password=officedepot@;Connect Timeout=4200";

            //Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["sConn"]);

        static void Main()
        {
            try
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("Scheduler Start At:" + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                DateTime dtDateFrom = new DateTime();
                DateTime dtDateTo = new DateTime();
                dtDateFrom = Common.GetMM_DD_YYYY("01/01/" + DateTime.Now.Year.ToString());
                DateTime now = DateTime.Now;
                string sDateTo = DateTime.Now.AddDays(-now.Day).ToString("dd'/'MM'/'yyyy");
                dtDateTo = Common.GetMM_DD_YYYY(sDateTo);                
                GenerateReport lGenerateReport = new GenerateReport();
                lGenerateReport.StartReportGeneration(dtDateFrom, dtDateTo);

                sbMessage.Append("Scheduler End At:" + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                GC.Collect();               
            }
            catch(Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
        }
     
    }
 }

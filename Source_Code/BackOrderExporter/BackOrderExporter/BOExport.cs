﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using BackOrderExporter.Class_Files;
using Microsoft.Reporting.WinForms;
using System.Globalization;

namespace BackOrderExporter
{
    public partial class BOExport : Form
    {
        #region Public Declaration
        List<FileInformation> lstFileInformation = new List<FileInformation>();
        List<string> lstFileType = new List<string>();
        List<ErrorList> lstSkippedRecords = new List<ErrorList>();
        private const int CP_NOCLOSE_BUTTON = 0x200;

        #endregion Public Declaration

        #region Constructor
        public BOExport()
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            InitializeComponent();
            btnSkippedRecords.Visible = false;
        }
        #endregion Constructor

        # region Events

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            btnSkippedRecords.Visible = false;
            lstSkippedRecords.Clear();
            string fileNameForLog = string.Empty;
            try
            {
                string msg = string.Empty;
                msg = IsAllFilesValid();
                if (!string.IsNullOrEmpty(msg))
                {
                    MessageBox.Show(msg, "BO Export");
                    return;
                }
                GC.Collect();
                Cursor.Current = Cursors.WaitCursor;
                List<ReportOutput> lstReportOutput = new List<ReportOutput>();
                foreach (var fileInfo in lstFileInformation)
                {
                    if (!File.Exists(fileInfo.Path))
                    {
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show(@"There is no file for the selected manufacturer.", "BO Export");
                        return;
                    }
                    else
                    {
                        string fileName = Path.GetFileNameWithoutExtension(fileInfo.Path).Trim();
                        var fileNameStr = fileName.Split('-');
                        fileNameForLog = fileName;


                        string preFix = string.Empty;
                        string reportId = string.Empty;

                        preFix = fileNameStr[0];
                        reportId = fileNameStr[1];

                        string csvContentStr = File.ReadAllText(fileInfo.Path);
                        string[] csvFileRecords = csvContentStr.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                        List<CsvFileInfo> lstReport = new List<CsvFileInfo>();

                        if (string.IsNullOrEmpty(reportId))
                        {
                            Cursor.Current = Cursors.Default;
                            MessageBox.Show(@"Incorrect file format.", "BO Export");
                            return;
                        }

                        if (reportId.Contains("BOR080"))
                        {
                            lstReport = ReadBOR080(csvFileRecords, preFix, fileName);
                        }
                        else if (reportId.Contains("BOR095"))
                        {
                            lstReport = ReadBOR095(csvFileRecords, preFix, fileName);
                        }
                        else if (reportId.Contains("BOR090"))
                        {
                            if (!csvFileRecords[0].Contains("SYSNAM ID-"))
                                lstReport = ReadBOR090UK(csvFileRecords, preFix, fileName);
                            else
                                lstReport = ReadBOR090(csvFileRecords, preFix, fileName);

                        }
                        else if (reportId.Contains("CBO553R"))
                        {
                            lstReport = ReadCBO553R(csvFileRecords, preFix, fileName);
                        }
                        else if (reportId.Contains("CBO551R"))
                        {
                            lstReport = ReadCBO551R(csvFileRecords, preFix, fileName);
                        }
                        else if (reportId.Contains("CBO552R"))
                        {
                            lstReport = ReadCBO552R(csvFileRecords, preFix, fileName);
                        }
                        else
                        {
                            Cursor.Current = Cursors.Default;
                            MessageBox.Show(@"Incorrect file format.", "BO Export");
                            return;
                        }

                        if (lstReport.Count > 0)
                            lstReportOutput.AddRange(CreateResult(lstReport, reportId, preFix));
                    }
                }
                if (lstSkippedRecords.Count > 0)
                {
                    btnSkippedRecords.Visible = true;
                }
                Cursor.Current = Cursors.Default;
                SendForDownload(lstReportOutput);
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception ex)
            {
                LogUtility.SaveLogEntry(ex, fileNameForLog, "Error in btnImport_Click Handler");
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {

            OpenFileDialog thisDialog = new OpenFileDialog();
            thisDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            thisDialog.Filter = "CSV files (*.csv)|*.csv";
            thisDialog.Title = "Browse Excel file";
            thisDialog.FilterIndex = 2;
            thisDialog.RestoreDirectory = true;
            thisDialog.Multiselect = true;
            thisDialog.Title = "Please Select Source File(s) for Conversion";

            btnSkippedRecords.Visible = false;
            lstSkippedRecords.Clear();

            if (thisDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (String filePath in thisDialog.FileNames)
                {
                    try
                    {
                        Stream myStream;
                        if ((myStream = thisDialog.OpenFile()) != null)
                        {
                            using (myStream)
                            {
                                lstFileType.Add(filePath);
                                var fileName = Path.GetFileName(filePath);
                                FileInformation objFileInformation = new FileInformation();
                                objFileInformation.Name = fileName;
                                objFileInformation.Path = filePath;
                                lstFileInformation.Add(objFileInformation);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    }
                }
                bindGrid();
            }

        }

        private void dgrdSelectedFiles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                btnSkippedRecords.Visible = false;
                lstSkippedRecords.Clear();
                var fileName = senderGrid.Rows[e.RowIndex].Cells[1].Value;
                var itemToRemove = lstFileInformation.Single(r => r.Name == fileName);
                lstFileInformation.Remove(itemToRemove);
                if (lstFileInformation.Count == 0)
                    lstFileType.Clear();
                bindGrid();
            }
        }

        private void BOExport_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSkippedRecords_Click(object sender, EventArgs e)
        {
            SkippedRecordsDownload();
        }

        # endregion Events

        #region Parsers
        public List<CsvFileInfo> ReadBOR080(string[] recordList, string preFix, string fileName)
        {
            List<CsvFileInfo> lstReport = new List<CsvFileInfo>();
            int rowNum = 0;
            foreach (string row in recordList)
            {
                rowNum++;
                try
                {
                    if (row.Trim().Length > 100)
                    {
                        var rowData = row.Replace("\r\n", string.Empty);
                        rowData = rowData.Replace("\"", string.Empty);
                        string b = rowData.Substring(0, 9);
                        int n;
                        bool isNumeric = int.TryParse(b, out n);
                        CsvFileInfo record;
                        var isDataRow080 = IsDataRow080(rowData);
                        if (isDataRow080)
                        {

                            record = new CsvFileInfo();
                            record.ItemNum = b.Trim();

                            float qty = 0;
                            var quantity = rowData.Substring(31, 7).Trim();
                            bool isQtyNumeric = float.TryParse(quantity, out qty);
                            record.Quantity = (isQtyNumeric ? float.Parse(quantity) : 0);

                            var CatelogNum = rowData.Substring(41, 15).Trim();
                            record.CatelogNum = CatelogNum.Substring(CatelogNum.IndexOf('-') + 1);

                            //record.date = SetDateFormat(rowData.Substring(88, 11).Trim());
                            record.inFile = preFix;
                            lstReport.Add(record);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorList obj = new ErrorList();
                    obj.rowNum = rowNum;
                    obj.FileName = fileName;
                    obj.ErrorRecord = row.Replace("\r\n", string.Empty).Replace("\"", string.Empty).Replace(",", string.Empty);
                    lstSkippedRecords.Add(obj);
                    LogUtility.SaveLogEntry(ex, fileName, obj.ErrorRecord);
                }
            }
            return lstReport;
        }

        public List<CsvFileInfo> ReadBOR090UK(string[] recordList, string preFix, string fileName)
        {
            string output = string.Empty;
            List<CsvFileInfo> lstReport = new List<CsvFileInfo>();
            int rowNum = 0;
            foreach (string row in recordList)
            {
                rowNum++;
                try
                {
                    if (row.Trim().Length > 100)
                    {
                        var rowData = row.Replace("\r\n", string.Empty);
                        rowData = rowData.Replace("\"", string.Empty);
                        string b = rowData.Substring(3, 9);
                        int n;
                        bool isNumeric = int.TryParse(b, out n);
                        CsvFileInfo record;
                        var isDataRow090UK = IsDataRow090UK(rowData);
                        if (isDataRow090UK)
                        {

                            record = new CsvFileInfo();
                            record.ItemNum = b.Trim();

                            float qty = 0;
                            var quantity = rowData.Substring(42, 7).Trim();
                            bool isQtyNumeric = float.TryParse(quantity, out qty);
                            record.Quantity = (isQtyNumeric ? float.Parse(quantity) : 0);

                            var CatelogNum = rowData.Substring(53, 18).Trim();
                            record.CatelogNum = CatelogNum.Substring(CatelogNum.IndexOf('-') + 1);
                            record.inFile = preFix;
                            //record.date = SetDateFormat(rowData.Substring(95, 13).Trim());
                            lstReport.Add(record);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorList obj = new ErrorList();
                    obj.rowNum = rowNum;
                    obj.FileName = fileName;
                    obj.ErrorRecord = row.Replace("\r\n", string.Empty).Replace("\"", string.Empty).Replace(",", string.Empty);
                    lstSkippedRecords.Add(obj);
                    LogUtility.SaveLogEntry(ex, fileName, obj.ErrorRecord);
                }
            }
            return lstReport;
        }

        public List<CsvFileInfo> ReadBOR095(string[] recordList, string preFix, string fileName)
        {
            List<CsvFileInfo> lstCsvFileInfo = new List<CsvFileInfo>();
            int rowNum = 0;
            foreach (string row in recordList)
            {
                rowNum++;
                try
                {
                    if (row.Trim().Length > 50)
                    {
                        var rowData = row.Replace("\r\n", string.Empty);
                        rowData = rowData.Replace("\"", string.Empty);
                        string b = rowData.Substring(0, 8);
                        int n;
                        bool isNumeric = int.TryParse(b, out n);
                        CsvFileInfo record;
                        var isDataRow095 = IsDataRow095(rowData);
                        if (isDataRow095)
                        {
                            record = new CsvFileInfo();
                            record.ItemNum = b.Trim();

                            float qty = 0;
                            var quantity = rowData.Substring(39, 7).Trim();
                            bool isQtyNumeric = float.TryParse(quantity, out qty);
                            record.Quantity = (isQtyNumeric ? float.Parse(quantity) : 0);

                            var CatelogNum = rowData.Substring(50, 18).Trim();
                            record.CatelogNum = CatelogNum.Substring(CatelogNum.IndexOf('-') + 1);
                            record.inFile = preFix;
                            //record.date = SetDateFormat(rowData.Substring(68, 37).Trim());
                            lstCsvFileInfo.Add(record);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorList obj = new ErrorList();
                    obj.rowNum = rowNum;
                    obj.FileName = fileName;
                    obj.ErrorRecord = row.Replace("\r\n", string.Empty).Replace("\"", string.Empty).Replace(",", string.Empty);
                    lstSkippedRecords.Add(obj);
                    LogUtility.SaveLogEntry(ex, fileName, obj.ErrorRecord);
                }
            }
            return lstCsvFileInfo;
        }

        public List<CsvFileInfo> ReadBOR090(string[] recordList, string preFix, string fileName)
        {
            List<CsvFileInfo> lstCsvFileInfo = new List<CsvFileInfo>();
            int rowNum = 0;
            foreach (string row in recordList)
            {
                rowNum++;
                try
                {
                    if (row.Trim().Length > 100)
                    {
                        var rowData = row.Replace("\r\n", string.Empty);
                        rowData = rowData.Replace("\"", string.Empty);
                        string b = rowData.Substring(0, 9);
                        int n;
                        bool isNumeric = int.TryParse(b, out n);
                        CsvFileInfo record;
                        var isDataRow090 = IsDataRow090(rowData);
                        if (isDataRow090)
                        {
                            record = new CsvFileInfo();
                            record.ItemNum = b.Trim();

                            float qty = 0;
                            var quantity = rowData.Substring(39, 7).Trim();
                            bool isQtyNumeric = float.TryParse(quantity, out qty);
                            record.Quantity = (isQtyNumeric ? float.Parse(quantity) : 0);

                            var CatelogNum = rowData.Substring(51, 19).Trim();
                            record.CatelogNum = CatelogNum.Substring(CatelogNum.IndexOf('-') + 1);
                            record.inFile = preFix;
                            //record.date = SetDateFormat(rowData.Substring(105, 13).Trim());
                            lstCsvFileInfo.Add(record);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorList obj = new ErrorList();
                    obj.rowNum = rowNum;
                    obj.FileName = fileName;
                    obj.ErrorRecord = row.Replace("\r\n", string.Empty).Replace("\"", string.Empty).Replace(",", string.Empty);
                    lstSkippedRecords.Add(obj);
                    LogUtility.SaveLogEntry(ex, fileName, obj.ErrorRecord);
                }
            }
            return lstCsvFileInfo;
        }

        public List<CsvFileInfo> ReadCBO553R(string[] recordList, string preFix, string fileName)
        {
            List<CsvFileInfo> lstCsvFileInfo = new List<CsvFileInfo>();
            int rowNum = 0;
            string date = string.Empty;
            foreach (string row in recordList)
            {
                rowNum++;
                try
                {
                    //if (rowNum == 0)
                    //{
                    //    string tobesearched = "Date : ";
                    //    date = row.Substring(row.IndexOf(tobesearched) + tobesearched.Length, 15).Trim();
                    //}
                    if (row.Trim().Length > 100)
                    {
                        var rowData = row.Replace("\r\n", string.Empty);
                        rowData = rowData.Replace("\"", string.Empty);
                        string b = rowData.Substring(0, 9);
                        int n;
                        bool isNumeric = int.TryParse(b, out n);
                        CsvFileInfo record;
                        var isDataRow553 = IsDataRow553(rowData);
                        if (isDataRow553)
                        {
                            record = new CsvFileInfo();
                            record.ItemNum = b.Trim();

                            float qty = 0;
                            var quantity = rowData.Substring(42, 7).Trim();
                            bool isQtyNumeric = float.TryParse(quantity, out qty);
                            record.Quantity = (isQtyNumeric ? float.Parse(quantity) : 0);

                            record.CatelogNum = rowData.Substring(56, 15).Trim();
                            record.inFile = preFix;
                            //record.date = date;
                            lstCsvFileInfo.Add(record);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorList obj = new ErrorList();
                    obj.rowNum = rowNum;
                    obj.FileName = fileName;
                    obj.ErrorRecord = row.Replace("\r\n", string.Empty).Replace("\"", string.Empty).Replace(",", string.Empty);
                    lstSkippedRecords.Add(obj);
                    LogUtility.SaveLogEntry(ex, fileName, obj.ErrorRecord);
                }
            }
            return lstCsvFileInfo;
        }

        public List<CsvFileInfo> ReadCBO551R(string[] recordList, string preFix, string fileName)
        {
            List<CsvFileInfo> lstCsvFileInfo = new List<CsvFileInfo>();
            int rowNum = 0;
            string date = string.Empty;
            foreach (string row in recordList)
            {
                rowNum++;
                try
                {
                    if (rowNum == 0)
                    {
                        string tobesearched = "Date : ";
                        date = row.Substring(row.IndexOf(tobesearched) + tobesearched.Length).Trim();
                    }
                    if (row.Trim().Length > 100 && rowNum > 5)
                    {
                        var rowData = row.Replace("\r\n", string.Empty);
                        rowData = rowData.Replace("\"", string.Empty);
                        string b = rowData.Substring(0, 9);
                        int n;
                        bool isNumeric = int.TryParse(b, out n);
                        CsvFileInfo record;
                        var isDataRow551 = IsDataRow551(rowData);
                        if (isDataRow551)
                        {
                            record = new CsvFileInfo();
                            record.ItemNum = rowData.Substring(0, 9).Trim();

                            float qty = 0;

                            var quantity = rowData.Substring(42, 7).Trim();
                            bool isQtyNumeric = float.TryParse(quantity, out qty);
                            record.Quantity = (isQtyNumeric ? float.Parse(quantity) : 0);


                            record.CatelogNum = rowData.Substring(56, 15).Trim();
                            record.inFile = preFix;
                            //record.date = SetDateFormat(rowData.Substring(107, 15).Trim());
                            lstCsvFileInfo.Add(record);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorList obj = new ErrorList();
                    obj.rowNum = rowNum;
                    obj.FileName = fileName;
                    obj.ErrorRecord = row.Replace("\r\n", string.Empty).Replace("\"", string.Empty).Replace(",", string.Empty);
                    lstSkippedRecords.Add(obj);
                    LogUtility.SaveLogEntry(ex, fileName, obj.ErrorRecord);
                }
            }
            return lstCsvFileInfo;
        }

        public List<CsvFileInfo> ReadCBO552R(string[] recordList, string preFix, string fileName)
        {
            List<CsvFileInfo> lstCsvFileInfo = new List<CsvFileInfo>();
            int rowNum = 0;
            foreach (string row in recordList)
            {
                rowNum++;
                try
                {
                    if (row.Trim().Length > 100)
                    {
                        var rowData = row.Replace("\r\n", string.Empty);
                        rowData = rowData.Replace("\"", string.Empty);
                        string b = rowData.Substring(0, 9);
                        int n;
                        bool isNumeric = int.TryParse(b, out n);
                        CsvFileInfo record;
                        var isDataRow552 = IsDataRow552(rowData);
                        if (isDataRow552)
                        {
                            record = new CsvFileInfo();
                            record.ItemNum = b.Trim();

                            float qty = 0;
                            var quantity = rowData.Substring(42, 7).Trim();
                            bool isQtyNumeric = float.TryParse(quantity, out qty);
                            record.Quantity = (isQtyNumeric ? float.Parse(quantity) : 0);

                            record.CatelogNum = rowData.Substring(56, 15).Trim();
                            //record.date = SetDateFormat(rowData.Substring(108, 12).Trim());
                            record.inFile = preFix;
                            lstCsvFileInfo.Add(record);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorList obj = new ErrorList();
                    obj.rowNum = rowNum;
                    obj.FileName = fileName;
                    obj.ErrorRecord = row.Replace("\r\n", string.Empty).Replace("\"", string.Empty).Replace(",", string.Empty);
                    lstSkippedRecords.Add(obj);
                    LogUtility.SaveLogEntry(ex, fileName, obj.ErrorRecord);
                }
            }
            return lstCsvFileInfo;
        }
        #endregion Parsers

        # region Methods

        public List<ReportOutput> CreateResult(List<CsvFileInfo> lstRecords, string fileName, string preFix)
        {
            foreach (var itm in lstRecords)
            {
                itm.CatelogNum = itm.CatelogNum.Replace("-XX", "");
            }
            var inFile = lstRecords.FirstOrDefault().inFile;
            FileSpecifications objFileSpecifications = new FileSpecifications();
            objFileSpecifications = GetFilespecification(preFix);
            string fileType = GetFileType(fileName);

            var query = (from row in lstRecords
                         group row by new { row.ItemNum, row.CatelogNum } into g
                         select new
                         {
                             g.Key.ItemNum,
                             VikingCode = g.Key.CatelogNum,
                             Quantity = g.Count().ToString(),
                             Sum = g.Sum(c => c.Quantity)
                         }).ToList();

            var res = (from r in query
                       select new ReportOutput
                       {
                           ODCode = r.ItemNum,
                           VikingCode = r.VikingCode,
                           NumberofBOs = r.Quantity,
                           QuanityOnBackOrder = r.Sum.ToString("0.000"),
                           Country = objFileSpecifications.Country,
                           date = DateTime.Now.ToString("dd-MMM-yy"),
                           Site = objFileSpecifications.OutputFile,
                           File = fileType
                       }).ToList();
            return res;
        }

        public string SetDateFormat(string dat)
        {
            string[] dtArr = dat.Split('/');
            DateTime result;
            try
            {
                if (dtArr[0].Length < 2)
                {
                    dtArr[0] = "0" + dtArr[0];
                }
                if (dtArr[1].Length < 2)
                {
                    dtArr[1] = "0" + dtArr[1];
                }
                result = DateTime.ParseExact(dtArr[0] + "/" + dtArr[1] + "/" + dtArr[2], "dd/MM/yy", CultureInfo.InvariantCulture);
                return result.ToString("dd-MMM-yyyy");
            }
            catch
            {
                return dat;
            }
        }

        public FileSpecifications GetFilespecification(string sitePrefix)
        {
            /*
             * SitePrefix	SiteNumber	CountryName
                ASH	            R	    UKandIRE
                AUS	            F	    Germany
                DUB	            E	    UKandIRE
                GOH	            F	    Germany
                LEI	            L	    UKandIRE
                LKN	            H	    Germany
                MDR	            ES	    Spain
                MSL	            W	    France
                NTH	            M	    UKandIRE
                SMC	            A	    France
                SUR	            P	    France
                SZ 	            SZ	    Italy
                ZWO	            Z	    Netherlands
                CZH             CZ      Czech Republic
                CHL             CH      Switzerland
                STR             SW      Sweden
             */
            FileSpecifications objFileSpecifications = new FileSpecifications();

            if (sitePrefix.ToUpper().Equals("ASH"))
            {
                objFileSpecifications.OutputFile = "R";
                objFileSpecifications.Country = "UKandIRE";
            }
            else if (sitePrefix.ToUpper().Equals("AUS"))
            {
                objFileSpecifications.OutputFile = "F";
                objFileSpecifications.Country = "Germany";
            }
            else if (sitePrefix.ToUpper().Equals("DUB"))
            {
                objFileSpecifications.OutputFile = "E";
                objFileSpecifications.Country = "UKandIRE";
            }
            else if (sitePrefix.ToUpper().Equals("GOH"))
            {
                objFileSpecifications.OutputFile = "F";
                objFileSpecifications.Country = "Germany";
            }
            else if (sitePrefix.ToUpper().Equals("LEI"))
            {
                objFileSpecifications.OutputFile = "L";
                objFileSpecifications.Country = "UKandIRE";
            }
            else if (sitePrefix.ToUpper().Equals("LKN"))
            {
                objFileSpecifications.OutputFile = "H";
                objFileSpecifications.Country = "Germany";
            }

            else if (sitePrefix.ToUpper().Equals("MDR"))
            {
                objFileSpecifications.OutputFile = "ES";
                objFileSpecifications.Country = "Spain";
            }
            else if (sitePrefix.ToUpper().Equals("MSL"))
            {
                objFileSpecifications.OutputFile = "W";
                objFileSpecifications.Country = "France";
            }
            else if (sitePrefix.ToUpper().Equals("NTH"))
            {
                objFileSpecifications.OutputFile = "M";
                objFileSpecifications.Country = "UKandIRE";
            }
            else if (sitePrefix.ToUpper().Equals("SMC"))
            {
                objFileSpecifications.OutputFile = "A";
                objFileSpecifications.Country = "France";
            }
            else if (sitePrefix.ToUpper().Equals("SUR"))
            {
                objFileSpecifications.OutputFile = "P";
                objFileSpecifications.Country = "France";
            }
            else if (sitePrefix.ToUpper().Equals("SZ"))
            {
                objFileSpecifications.OutputFile = "SZ";
                objFileSpecifications.Country = "Italy";
            }
            else if (sitePrefix.ToUpper().Equals("ZWO"))
            {
                objFileSpecifications.OutputFile = "Z";
                objFileSpecifications.Country = "Netherlands";
            }
            else if (sitePrefix.ToUpper().Equals("STR"))
            {
                objFileSpecifications.OutputFile = "SW";
                objFileSpecifications.Country = "Sweden";
            }
            else if (sitePrefix.ToUpper().Equals("CZH"))
            {
                objFileSpecifications.OutputFile = "CZ";
                objFileSpecifications.Country = "Czech Republic";
            }
            else if (sitePrefix.ToUpper().Equals("CHL"))
            {
                objFileSpecifications.OutputFile = "CH";
                objFileSpecifications.Country = "Switzerland";
            }
            else
            {
                objFileSpecifications.OutputFile = "NA";
                objFileSpecifications.Country = "NA";
            }
            return objFileSpecifications;
        }

        private bool IsPrefixValid(string prefix)
        {
            bool result = false;
            switch (prefix)
            {
                case "ASH":
                case "AUS":
                case "DUB":
                case "GOH":
                case "LEI":
                case "LKN":
                case "MDR":
                case "MSL":
                case "NTH":
                case "SMC":
                case "SUR":
                case "SZ":
                case "ZWO":
                case "STR":
                case "CZH":
                case "CHL":
                    result = true;
                    break;
            }
            return result;
        }

        private bool IsReportIdValid(string reportId)
        {
            bool result = false;
            switch (reportId)
            {
                case "BOR080":
                case "BOR090":
                case "BOR095":
                case "CBO551R":
                case "CBO552R":
                case "CBO553R":
                    result = true;
                    break;
            }
            return result;
        }

        private bool IsDataRow551(string strRow)
        {
            bool res = false;
            string itemNo = string.Empty;
            string custNo = string.Empty;
            string orderNo = string.Empty;
            string line = string.Empty;
            string CatelogNum = string.Empty;


            itemNo = strRow.Substring(0, 9).Trim().TrimEnd('-');
            custNo = strRow.Substring(12, 9).Trim().TrimEnd('-');
            orderNo = strRow.Substring(22, 9).Trim().TrimEnd('-');
            line = strRow.Substring(36, 7).Trim().TrimEnd('-');
            CatelogNum = strRow.Substring(56, 15).Trim().TrimEnd('-');

            int numLine;
            bool isNumericLine = int.TryParse(line, out numLine);

            if ((!string.IsNullOrEmpty(itemNo)) && (!string.IsNullOrEmpty(custNo)) && (!string.IsNullOrEmpty(orderNo)) && (!string.IsNullOrEmpty(line)) && (!string.IsNullOrEmpty(CatelogNum)) && isNumericLine)
            {
                res = true;
            }
            return res;
        }

        private bool IsDataRow552(string strRow)
        {
            bool res = false;
            string itemNo = string.Empty;
            string custNo = string.Empty;
            string orderNo = string.Empty;
            string line = string.Empty;
            string CatelogNum = string.Empty;


            itemNo = strRow.Substring(0, 9).Trim().TrimEnd('-');
            custNo = strRow.Substring(12, 9).Trim().TrimEnd('-');
            orderNo = strRow.Substring(22, 9).Trim().TrimEnd('-');
            line = strRow.Substring(36, 7).Trim().TrimEnd('-');
            CatelogNum = strRow.Substring(56, 15).Trim().TrimEnd('-');

            int numLine;
            bool isNumericLine = int.TryParse(line, out numLine);

            if ((!string.IsNullOrEmpty(itemNo)) && (!string.IsNullOrEmpty(custNo)) && (!string.IsNullOrEmpty(orderNo)) && (!string.IsNullOrEmpty(line)) && (!string.IsNullOrEmpty(CatelogNum)) && isNumericLine)
            {
                res = true;
            }
            return res;
        }

        private bool IsDataRow553(string strRow)
        {
            bool res = false;
            string itemNo = string.Empty;
            string custNo = string.Empty;
            string orderNo = string.Empty;
            string line = string.Empty;
            string CatelogNum = string.Empty;

            itemNo = strRow.Substring(0, 9).Trim().TrimEnd('-');
            custNo = strRow.Substring(12, 9).Trim().TrimEnd('-');
            orderNo = strRow.Substring(22, 9).Trim().TrimEnd('-');
            line = strRow.Substring(36, 7).Trim().TrimEnd('-');
            CatelogNum = strRow.Substring(56, 15).Trim().TrimEnd('-');

            int numLine;
            bool isNumericLine = int.TryParse(line, out numLine);

            if ((!string.IsNullOrEmpty(itemNo)) && (!string.IsNullOrEmpty(custNo)) && (!string.IsNullOrEmpty(orderNo)) && (!string.IsNullOrEmpty(line)) && (!string.IsNullOrEmpty(CatelogNum)) && isNumericLine)
            {
                res = true;
            }
            return res;
        }

        private bool IsDataRow080(string strRow)
        {
            bool res = false;
            string itemNo = string.Empty;
            string custNo = string.Empty;
            string invNo = string.Empty;
            string line = string.Empty;
            string catelogNum = string.Empty;

            itemNo = strRow.Substring(0, 9).Trim().TrimEnd('-');
            custNo = strRow.Substring(9, 9).Trim().TrimEnd('-');
            invNo = strRow.Substring(19, 9).Trim().TrimEnd('-');
            line = strRow.Substring(27, 7).Trim().TrimEnd('-');
            catelogNum = strRow.Substring(41, 15).Trim().Trim('-');

            int numLine;
            bool isNumericLine = int.TryParse(line, out numLine);

            if ((!string.IsNullOrEmpty(itemNo)) && (!string.IsNullOrEmpty(custNo)) && (!string.IsNullOrEmpty(invNo)) && (!string.IsNullOrEmpty(line)) && (!string.IsNullOrEmpty(catelogNum)) && isNumericLine)
            {
                res = true;
            }
            return res;
        }

        private bool IsDataRow090(string strRow)
        {
            bool res = false;
            string itemNo = string.Empty;
            string custNo = string.Empty;
            string invNo = string.Empty;
            string line = string.Empty;
            string CatelogNum = string.Empty;

            itemNo = strRow.Substring(0, 9).Trim().TrimEnd('-');
            custNo = strRow.Substring(9, 9).Trim().TrimEnd('-');
            invNo = strRow.Substring(19, 9).Trim().TrimEnd('-');
            line = strRow.Substring(27, 7).Trim().TrimEnd('-');
            CatelogNum = strRow.Substring(51, 19).Trim().TrimEnd('-');

            int numLine;
            bool isNumericLine = int.TryParse(line, out numLine);

            if ((!string.IsNullOrEmpty(itemNo)) && (!string.IsNullOrEmpty(custNo)) && (!string.IsNullOrEmpty(invNo)) && (!string.IsNullOrEmpty(line)) && (!string.IsNullOrEmpty(CatelogNum)) && isNumericLine)
            {
                res = true;
            }
            return res;
        }

        private bool IsDataRow090UK(string strRow)
        {
            bool res = false;
            string itemNo = string.Empty;
            string custNo = string.Empty;
            string invNo = string.Empty;
            string line = string.Empty;
            string CatelogNum = string.Empty;

            itemNo = strRow.Substring(3, 9).Trim().TrimEnd('-');
            custNo = strRow.Substring(13, 9).Trim().TrimEnd('-');
            invNo = strRow.Substring(22, 9).Trim().TrimEnd('-');
            line = strRow.Substring(31, 7).Trim().TrimEnd('-');
            CatelogNum = strRow.Substring(53, 18).Trim().Trim('-');

            int numLine;
            bool isNumericLine = int.TryParse(line, out numLine);

            if ((!string.IsNullOrEmpty(itemNo)) && (!string.IsNullOrEmpty(custNo)) && (!string.IsNullOrEmpty(invNo)) && (!string.IsNullOrEmpty(line)) && (!string.IsNullOrEmpty(CatelogNum)) && isNumericLine)
            {
                res = true;
            }
            return res;
        }

        private bool IsDataRow095(string strRow)
        {
            bool res = false;
            string itemNo = string.Empty;
            string custNo = string.Empty;
            string orderNo = string.Empty;
            string line = string.Empty;
            string CatelogNum = string.Empty;


            itemNo = strRow.Substring(0, 9).Trim().TrimEnd('-');
            custNo = strRow.Substring(9, 9).Trim().TrimEnd('-');
            orderNo = strRow.Substring(27, 9).Trim().TrimEnd('-');
            line = strRow.Substring(36, 7).Trim().TrimEnd('-');
            CatelogNum = strRow.Substring(50, 18).Trim().TrimEnd('-');

            int numLine;
            bool isNumericLine = int.TryParse(line, out numLine);

            if ((!string.IsNullOrEmpty(itemNo)) && (!string.IsNullOrEmpty(custNo)) && (!string.IsNullOrEmpty(orderNo)) && (!string.IsNullOrEmpty(line)) && (!string.IsNullOrEmpty(CatelogNum)) && isNumericLine)
            {
                res = true;
            }
            return res;
        }

        private string IsAllFilesValid()
        {
            string message = string.Empty;
            List<string> lstValidatedFiles = new List<string>();
            foreach (FileInformation fileInfo in lstFileInformation)
            {
                if (lstValidatedFiles.Contains(fileInfo.Name))
                {
                    continue;
                }

                string msg = string.Empty;
                string hMsg = "Error for File " + fileInfo.Name + "\n";
                try
                {
                    var fileInfos = File.ReadAllText(fileInfo.Path);
                }
                catch
                {
                    msg += "-File already opened. Please close this file and  try again.\n";
                }

                if (!fileInfo.Path.Contains("-"))
                {
                    lstValidatedFiles.Add(fileInfo.Name);
                    msg += "-Invalid file name.\n";
                    if (!string.IsNullOrEmpty(msg))
                    {
                        message += hMsg;
                        message += msg + "\n";
                    }
                    continue;
                }

                var fileNameStr = Path.GetFileNameWithoutExtension(fileInfo.Path).Trim().Split('-');
                string preFix = string.Empty;
                string reportId = string.Empty;

                preFix = fileNameStr[0];
                reportId = fileNameStr[1];

                if (!IsPrefixValid(preFix))
                {
                    msg += "-Site Prefix '" + preFix.Trim() + "' not valid.\n";
                }
                if (!IsReportIdValid(reportId))
                {
                    msg += "-Report Id '" + reportId.Trim() + "' not valid.\n";
                }
                var fileCount = lstFileInformation.Where(x => x.Name.ToLower() == fileInfo.Name.ToLower()).Count();
                var isFileExist = lstFileInformation.Any(x => x.Name.ToLower() == fileInfo.Name.ToLower());
                if (fileCount > 1)
                {
                    msg += "-File already exist.\n";
                }

                var fileType = GetFileType(fileInfo.Name);
                if (fileType.Equals("Wrong"))
                {
                    msg += "-Wrong file type.\n";
                }
                if (!string.IsNullOrEmpty(msg))
                {
                    message += hMsg;
                    message += msg + "\n";
                }
                lstValidatedFiles.Add(fileInfo.Name);
            }
            int dailyFilesCount = 0;
            int totalFilesCount = 0;
            foreach (var fileName in lstValidatedFiles)
            {
                if (GetFileType(fileName) == "Daily")
                {
                    dailyFilesCount++;
                }
                else if (GetFileType(fileName) == "Total")
                {
                    totalFilesCount++;
                }
            }
            if (dailyFilesCount > 0 && totalFilesCount > 0)
            {
                message += "\nWrong file type. Please import same type of files.";
            }

            return message;
        }

        private void bindGrid()
        {
            FileInformation obj = new FileInformation();
            dgrdSelectedFiles.DataSource = obj;
            dgrdSelectedFiles.DataSource = lstFileInformation;
            if (dgrdSelectedFiles.Columns["Name"] != null)
            {
                dgrdSelectedFiles.Columns["Name"].DisplayIndex = 0;
                dgrdSelectedFiles.Columns["Name"].Width = 150;
            }
            if (dgrdSelectedFiles.Columns["Path"] != null)
            {
                dgrdSelectedFiles.Columns["Path"].DisplayIndex = 1;
                dgrdSelectedFiles.Columns["Path"].Width = 300;
            }
            if (dgrdSelectedFiles.Columns["Remove"] != null)
            {
                dgrdSelectedFiles.Columns["Remove"].DisplayIndex = 2;
                ((System.Windows.Forms.DataGridViewButtonColumn)(dgrdSelectedFiles.Columns["Remove"])).Text = "Remove";
            }
        }

        private string GetFileType(string fileName)
        {
            string fileType = string.Empty;
            if (fileName.Contains("BOR080") || fileName.Contains("CBO553R"))
                fileType = "Daily";
            else if (fileName.Contains("BOR090") || fileName.Contains("CBO551R") || fileName.Contains("CBO552R") || fileName.Contains("BOR095"))
                fileType = "Total";
            else
                fileType = "Wrong";
            return fileType;
        }

        private void SendForDownload(List<ReportOutput> lstReportOutput)
        {
            if (lstReportOutput.Count > 1)
            {
                this.reportViewer1.Clear();

                DataTable dt = ExportListToExcel.ConvertToDatatable(lstReportOutput);
                string fileName = "BackOrder" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";

                SaveFileDialog SaveFile = new SaveFileDialog();
                SaveFile.Title = "Save xls File As";
                SaveFile.DefaultExt = "xls";
                SaveFile.FileName = fileName;
                SaveFile.Filter = "xls files (*.xls)|*.xls|files (*.*)|*.*";
                SaveFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                SaveFile.OverwritePrompt = true;
                if (SaveFile.FileName != "")
                {
                    if (SaveFile.ShowDialog() == DialogResult.OK)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        this.reportViewer1.LocalReport.DataSources.Clear();


                        ReportDataSource rds = new ReportDataSource("dtBackOrder", dt);
                        this.reportViewer1.Clear();
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "BackOrderExporter.RDLC.BOExport.rdlc";
                        this.reportViewer1.LocalReport.DataSources.Add(rds);
                        this.reportViewer1.RefreshReport();


                        Warning[] warnings;
                        string[] streamids;
                        string mimeType;
                        string encoding;
                        string extension;
                        byte[] bytes = this.reportViewer1.LocalReport.Render("Excel", "",
                           out mimeType, out encoding, out extension, out streamids, out warnings);
                        //create file stream in create mode

                        FileStream fs = new FileStream(SaveFile.FileName, FileMode.Create);
                        fs.SetLength(0);
                        //create Excel file
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();

                        // Code for Open File after download
                        Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                        excelApp.Visible = true;
                        string workbookPath = (SaveFile.FileName);
                        Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApp.Workbooks.Open(workbookPath,
                            0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "",
                            true, false, 0, true, false, false);

                        Cursor.Current = Cursors.Default;
                    }
                }
            }
            else
            {
                MessageBox.Show(@"No record found.", "BO Export");
                return;
            }
        }

        private void SkippedRecordsDownload()
        {
            if (lstSkippedRecords.Count > 0)
            {
                this.reportViewer1.Clear();

                DataTable dt = ExportListToExcel.ConvertToDatatable(lstSkippedRecords);
                string fileName = "BackOrder Skipped Records" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";

                SaveFileDialog SaveFile = new SaveFileDialog();
                SaveFile.Title = "Save xls File As";
                SaveFile.DefaultExt = "xls";
                SaveFile.FileName = fileName;
                SaveFile.Filter = "xls files (*.xls)|*.xls|files (*.*)|*.*";
                SaveFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                SaveFile.OverwritePrompt = true;
                if (SaveFile.FileName != "")
                {
                    if (SaveFile.ShowDialog() == DialogResult.OK)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        this.reportViewer1.LocalReport.DataSources.Clear();


                        ReportDataSource rds = new ReportDataSource("dtErrorList", dt);
                        this.reportViewer1.Clear();
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "BackOrderExporter.RDLC.ErrorList.rdlc";
                        this.reportViewer1.LocalReport.DataSources.Add(rds);
                        this.reportViewer1.RefreshReport();


                        Warning[] warnings;
                        string[] streamids;
                        string mimeType;
                        string encoding;
                        string extension;
                        byte[] bytes = this.reportViewer1.LocalReport.Render("Excel", "",
                           out mimeType, out encoding, out extension, out streamids, out warnings);
                        //create file stream in create mode

                        FileStream fs = new FileStream(SaveFile.FileName, FileMode.Create);
                        fs.SetLength(0);
                        //create Excel file
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();

                        // Code for Open File after download
                        Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                        excelApp.Visible = true;
                        string workbookPath = (SaveFile.FileName);
                        Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApp.Workbooks.Open(workbookPath,
                            0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "",
                            true, false, 0, true, false, false);

                        Cursor.Current = Cursors.Default;
                    }
                }
            }
            else
            {
                MessageBox.Show(@"No record found.", "BO Export");
                return;
            }
        }

        # endregion Methods
    }
}

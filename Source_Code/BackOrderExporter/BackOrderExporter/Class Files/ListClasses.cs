﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace BackOrderExporter
{
    public class FileInformation
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }

    public class ErrorList
    {
        public int rowNum { get; set; }
        public string FileName { get; set; }
        public string ErrorRecord { get; set; }
    }

    public class FileSpecifications
    {
        public string Country { get; set; }
        public string FileType { get; set; }
        public string SiteName { get; set; }
        public string OutputFile { get; set; }
    }

    public class CsvFileInfo
    {
        public string ItemNum { get; set; }
        public float Quantity { get; set; }
        public string CatelogNum { get; set; }
        public string date { get; set; }
        public string inFile { get; set; }
        public string Site { get; set; }
    }

    public class ReportOutput
    {
        public string ODCode { get; set; }
        public string VikingCode { get; set; }
        public string NumberofBOs { get; set; }
        public string QuanityOnBackOrder { get; set; }
        public string date { get; set; }
        public string Country { get; set; }
        public string Site { get; set; }
        public string File { get; set; }
    }
}

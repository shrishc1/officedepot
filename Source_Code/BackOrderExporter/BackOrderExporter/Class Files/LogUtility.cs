﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace BackOrderExporter.Class_Files
{
    public class LogUtility
    {
        public static bool blnErrorLog = true;

        public static string Terminal = System.Environment.MachineName;

        public static bool SaveLogEntry(Exception ex, string fileName, string ErrorRecord)
        {
            try
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("***************************** " + System.DateTime.Now + " *****************************");
                sbMessage.Append("\r\n");
                sbMessage.Append("Machine Name: " + Terminal);
                sbMessage.Append("\r\n");
                sbMessage.Append("Error in File: " + fileName);
                sbMessage.Append("\r\n");
                sbMessage.Append("Error Record Info: " + ErrorRecord);
                sbMessage.Append("\r\n");
                sbMessage.Append("ErrorMessage: " + ex.Message + " |      Error Source:- " + ex.Source);
                sbMessage.Append("\r\n");
                sbMessage.Append("Error At: " + ex.StackTrace);
                sbMessage.Append("\r\n");
                sbMessage.Append("\r\n");

                bool flag = WriteToLog(sbMessage);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static bool WriteToLog(StringBuilder sbMessage)
        {
            try
            {                
                FileStream fs;
                string strLogFileName = "BackOrderExporter_Terminal_" + Terminal.ToString() + "_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
                string LogPath = AppDomain.CurrentDomain.BaseDirectory;
                string strLogFilePath = LogPath + "Logs\\";
                if (!Directory.Exists(strLogFilePath))
                    Directory.CreateDirectory(strLogFilePath);

                if (File.Exists(strLogFilePath + strLogFileName) == true) { fs = File.Open(strLogFilePath + strLogFileName, FileMode.Append, FileAccess.Write); }
                else { fs = File.Create(strLogFilePath + strLogFileName); }
                {
                    using (StreamWriter sw = new StreamWriter(fs)) { sw.Write(sbMessage.ToString() + Environment.NewLine); }
                    fs.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
